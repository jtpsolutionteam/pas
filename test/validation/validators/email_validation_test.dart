import 'package:flutter_test/flutter_test.dart';
import 'package:pas/validation/validators/validators.dart';

void main() {
  EmailValidation sut;

  setUp(() {
    sut = EmailValidation('any_value');
  });

  test('Deve retornar null se o email for vazio', () {
    expect(sut.validate(''), null);
  });

  test('Deve retornar null se o email for nulo', () {
    expect(sut.validate(null), null);
  });

  test('Deve retornar null se o email for válido', () {
    expect(sut.validate('rbmelolima@gmail.com'), null);
  });

  test('Deve retornar um erro de o email for inválido', () {
    expect(sut.validate('invalid_email'), 'Campo inválido');
  });
}
