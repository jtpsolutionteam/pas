import 'package:flutter_test/flutter_test.dart';
import 'package:pas/validation/validators/validators.dart';

void main() {
  RequiredFieldValidation sut;

  setUp(() {
    sut = RequiredFieldValidation('any_field');
  });

  test('Deve retornar null se o valor não for vazio', () {
    expect(sut.validate('any_value'), null);
  });

  test('Deve retornar um erro se o valor for vazio', () {
    expect(sut.validate(''), 'Campo obrigatório');
  });

  test('Deve retornar um erro se o valor for nulo', () {
    expect(sut.validate(null), 'Campo obrigatório');
  });
}
