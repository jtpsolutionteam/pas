import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/presentation/presenters/presenters.dart';

import '../../test.dart';

class IndexBatchShipmentImagesSpy extends Mock implements IndexBatchShipmentImages {}

class DeleteBatchShipmentImagesSpy extends Mock implements DeleteBatchShipmentImages {}

void main() {
  GetxUploadImageShipmentPresenter sut;
  UploadImagesSpy uploadImages;
  IndexBatchShipmentImagesSpy indexImages;
  DeleteBatchShipmentImagesSpy deleteImages;
  RemoveCurrentAccountSpy removeCurrentAccount;
  int cdProposta;
  int idElement;
  List<String> images;

  PostExpectation mockUploadSpawningImage() {
    return when(uploadImages.upload(
      cdProposta: anyNamed('cdProposta'),
      imagesInBase64: anyNamed('imagesInBase64'),
    ));
  }

  void mockUploadSpawningImageError(DomainError error) {
    return mockUploadSpawningImage().thenThrow(error);
  }

  PostExpectation mockIndexSpawningImages() {
    return when(indexImages.index());
  }

  void mockIndexSpawningImagesError(dynamic error) {
    mockIndexSpawningImages().thenThrow(error);
  }

  setUp(() {
    uploadImages = UploadImagesSpy();
    indexImages = IndexBatchShipmentImagesSpy();
    deleteImages = DeleteBatchShipmentImagesSpy();
    removeCurrentAccount = RemoveCurrentAccountSpy();

    cdProposta = 1235;
    idElement = 054;
    images = ['', '', '', ''];

    sut = GetxUploadImageShipmentPresenter(
      deleteSpawningImages: deleteImages,
      getSpawningImages: indexImages,
      removeCurrentAccount: removeCurrentAccount,
      uploadSpawningImages: uploadImages,
    );
  });

  group('UploadSpawningImages', () {
    test('Should call UploadSpawningImages with correct values and call DeleteSpawningImages after HTTP Status 200', () async {
      expectLater(sut.isUploadSuccess, emits(true));

      await sut.uploadImages(cdProposta: cdProposta, imagesInBase64: images, idElement: idElement);

      verify(uploadImages.upload(
        cdProposta: cdProposta,
        imagesInBase64: images,
      ));

      verify(deleteImages.delete(idElement));
    });

    test('Should go to login page on UploadSpawningImages throws Invalid Credentials', () async {
      mockUploadSpawningImageError(DomainError.invalidCredentials);

      sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/login')));

      await sut.uploadImages(
        cdProposta: cdProposta,
        imagesInBase64: images,
        idElement: idElement,
      );

      verify(removeCurrentAccount.remove());
    });
  });

  group('IndexSpawningImages', () {
    test('Should call IndexSpawningImages', () async {
      await sut.index();
      verify(indexImages.index());
    });

    test('Should throw UnexpectedError if IndexSpawningImages throws', () async {
      mockIndexSpawningImagesError(DomainError.unexpected);
      final future = sut.index();
      expect(future, throwsA(DomainError.unexpected));
    });
  });

  group('UploadAllImages', () {
    test('Shoul call uploadImages the correct number of times', () async {
      final spawning = BatchShipmentWithImagesModel(
        txProposta: faker.lorem.word(),
        dateCreate: DateTime.now().toString(),
        cdProposta: cdProposta,
        imagesInBase64: images,
        id: faker.randomGenerator.integer(999),
      );

      final list = [spawning, spawning, spawning, spawning];

      expectLater(sut.isLoading, emitsInOrder([true, false]));
      expectLater(sut.isUploadAllSuccess, emits(true));

      await sut.uploadAllImages(list);

      verify(uploadImages.upload(
        imagesInBase64: anyNamed('imagesInBase64'),
        cdProposta: anyNamed('cdProposta'),
      )).called(list.length);

      verify(deleteImages.delete(any)).called(list.length);
    });
  });
}
