import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/presentation/presenters/presenters.dart';

import '../../test.dart';

void main() {
  GetxLoginPresenter sut;
  ValidationSpy validation;
  AuthenticationSpy authentication;
  SaveCurrentAccountSpy saveCurrentAccount;
  String email;
  String password;
  String token;
  int idUser;

  PostExpectation mockValidationCall(String field) => when(validation.validate(
        field: field == null ? anyNamed('field') : field,
        value: anyNamed('value'),
      ));

  PostExpectation mockAuthenticationCall() => when(authentication.auth(any));

  PostExpectation mockSaveCurrentAccountCall() => when(saveCurrentAccount.save(any));

  void mockValidation({String field, String value}) {
    mockValidationCall(field).thenReturn(value);
  }

  void mockAuthentication() {
    mockAuthenticationCall().thenAnswer(
      (_) async => AccountEntity(token: token, id: idUser),
    );
  }

  void mockAuthenticationError(DomainError error) {
    mockAuthenticationCall().thenThrow(error);
  }

  void mockSaveCurrentAccountError() {
    mockSaveCurrentAccountCall().thenThrow(DomainError.unexpected);
  }

  setUp(() {
    validation = ValidationSpy();
    authentication = AuthenticationSpy();
    saveCurrentAccount = SaveCurrentAccountSpy();

    email = faker.internet.email();
    password = faker.internet.password();
    token = faker.guid.guid();
    idUser = 123;

    sut = GetxLoginPresenter(
      validation: validation,
      authentication: authentication,
      saveCurrentAccount: saveCurrentAccount,
    );

    //Mock sucess
    mockValidation();
    mockAuthentication();
  });

  group('Authentication', () {
    test('Garantir a chamada do Authentication com os valores corretos', () async {
      sut.validateEmail(email);
      sut.validatePassword(password);

      await sut.auth();

      verify(authentication.auth(
        AuthenticationParams(email: email, password: password),
      )).called(1);
    });
  });

  group('SaveCurrentAccount', () {
    test('Garantir a chamada do SaveCurrentAccount com os valores corretos', () async {
      sut.validateEmail(email);
      sut.validatePassword(password);

      await sut.auth();

      verify(saveCurrentAccount.save(AccountEntity(
        token: token,
        id: idUser,
      ))).called(1);
    });
  });

  group('Validation', () {
    test('Garantir a chamada do Validation com o email correto', () {
      sut.validateEmail(email);
      verify(validation.validate(field: 'email', value: email)).called(1);
    });

    test('Emitir um EmailError se a validação falhar', () {
      mockValidation(value: 'error');

      sut.emailErrorStream.listen(expectAsync1((error) => expect(error, 'error')));

      sut.isFormValidStream.listen(expectAsync1((isValid) => expect(isValid, false)));

      sut.validateEmail(email);
      sut.validateEmail(email);
    });

    test('Emitir null se a validação do email for concluída com sucesso', () {
      sut.emailErrorStream.listen(expectAsync1((error) => expect(error, null)));

      sut.isFormValidStream.listen(expectAsync1((isValid) => expect(isValid, false)));

      sut.validateEmail(email);
      sut.validateEmail(email);
    });

    test('Garantir a chamada do Validation com a senha correta', () {
      sut.validatePassword(password);
      verify(validation.validate(field: 'password', value: password)).called(1);
    });

    test('Emitir um PasswordError se a validação falhar', () {
      mockValidation(value: 'error');

      sut.passwordErrorStream.listen(expectAsync1((error) => expect(error, 'error')));

      sut.isFormValidStream.listen(expectAsync1((isValid) => expect(isValid, false)));

      sut.validatePassword(password);
      sut.validatePassword(password);
    });

    test('Emitir null se a validação da senha for concluída com sucesso', () {
      sut.passwordErrorStream.listen(expectAsync1((error) => expect(error, null)));

      sut.isFormValidStream.listen(expectAsync1((isValid) => expect(isValid, false)));

      sut.validatePassword(password);
      sut.validatePassword(password);
    });

    test('Emitir um FormInvalid se qualquer campo for inválido', () {
      mockValidation(field: 'email', value: 'error');

      sut.emailErrorStream.listen(expectAsync1((error) => expect(error, 'error')));

      sut.passwordErrorStream.listen(expectAsync1((error) => expect(error, null)));

      sut.isFormValidStream.listen(expectAsync1((isValid) => expect(isValid, false)));

      sut.validateEmail(email);
      sut.validatePassword(password);
    });

    test('Emitir um FormValid se todos os campos forem validados com sucesso', () async {
      sut.emailErrorStream.listen(expectAsync1((error) => expect(error, null)));

      sut.passwordErrorStream.listen(expectAsync1((error) => expect(error, null)));

      expectLater(sut.isFormValidStream, emitsInOrder([false, true]));

      sut.validateEmail(email);
      await Future.delayed(Duration.zero);
      sut.validatePassword(password);
    });
  });

  group('Events', () {
    test('Garantir a emissão dos eventos corretos se a Authentication for concluída com sucesso', () async {
      sut.validateEmail(email);
      sut.validatePassword(password);

      expectLater(sut.isLoadingStream, emits(true));

      await sut.auth();
    });

    test('Garantir a mudança de página', () async {
      sut.validateEmail(email);
      sut.validatePassword(password);

      sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/home')));

      await sut.auth();
    });

    test('Garantir a emissão dos eventos corretos se ocorrer um InvalidCredentialsError', () async {
      mockAuthenticationError(DomainError.invalidCredentials);

      sut.validateEmail(email);
      sut.validatePassword(password);

      expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
      expectLater(
          sut.mainErrorStream,
          emitsInOrder(
            [null, DomainError.invalidCredentials],
          ));

      await sut.auth();
    });

    test('Garantir a emissão dos eventos corretos se ocorrer um UnexpectedError', () async {
      mockAuthenticationError(DomainError.unexpected);

      sut.validateEmail(email);
      sut.validatePassword(password);

      expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
      expectLater(
          sut.mainErrorStream,
          emitsInOrder(
            [null, DomainError.unexpected],
          ));

      await sut.auth();
    });

    test('Lançar um UnexpectedError se o SaveCurrentAccount falhar', () async {
      mockSaveCurrentAccountError();

      sut.validateEmail(email);
      sut.validatePassword(password);

      expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
      expectLater(
          sut.mainErrorStream,
          emitsInOrder(
            [null, DomainError.unexpected],
          ));

      await sut.auth();
    });
  });
}
