import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/domain/entities/account_entity.dart';
import 'package:pas/presentation/presenters/presenters.dart';

import '../../test.dart';

void main() {
  GetxSplashPresenter sut;
  LoadCurrentAccountSpy loadCurrentAccount;

  PostExpectation mockLoadCurrentAccountCall() {
    return when(loadCurrentAccount.load());
  }

  void mockLoadCurrentAccount({AccountEntity account}) {
    mockLoadCurrentAccountCall().thenAnswer((_) async => account);
  }

  void mockLoadCurrentAccountError() {
    mockLoadCurrentAccountCall().thenThrow(Exception());
  }

  setUp(() {
    loadCurrentAccount = LoadCurrentAccountSpy();
    sut = GetxSplashPresenter(loadCurrentAccount: loadCurrentAccount);
    mockLoadCurrentAccount(
      account: AccountEntity(
        token: faker.guid.guid(),
        id: 123,
      ),
    );
  });

  test('Garantir a chamada do LoadCurrentAccount', () async {
    await sut.checkAccount(durationInSeconds: 0);
    verify(loadCurrentAccount.load()).called(1);
  });

  test('Ir para a página inicial em caso de sucesso', () async {
    sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/home')));
    await sut.checkAccount(durationInSeconds: 0);
  });

  test('Ir para a página de login caso o resultado for nulo', () async {
    mockLoadCurrentAccount(account: null);
    sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/login')));
    await sut.checkAccount(durationInSeconds: 0);
  });

  test('Ir para a página de login caso o resultado for um erro', () async {
    mockLoadCurrentAccountError();
    sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/login')));
    await sut.checkAccount(durationInSeconds: 0);
  });
}
