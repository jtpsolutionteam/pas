import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/presentation/presenters/presenters.dart';

import '../../mocks/mocks.dart';
import '../../test.dart';

void main() {
  GetxBatchSavePresenter sut;
  InsertSpawningImagesSpy saveSpawningImagesSpy;
  BatchEntity batch;
  List<String> imagesInBase64;

  setUp(() {
    saveSpawningImagesSpy = InsertSpawningImagesSpy();
    batch = BatchEntity.fromMap(mockBatch);
    imagesInBase64 = ['', '', ''];
    sut = GetxBatchSavePresenter(
      batch: batch,
      saveSpawningImages: saveSpawningImagesSpy,
    );
  });

  mockSaveImagesError() {
    when(saveSpawningImagesSpy.insert(
      cdProposta: anyNamed('cdProposta'),
      images: anyNamed('images'),
      bl: anyNamed('bl'),
    )).thenThrow(new Exception());
  }

  test('Garantir a chamada do InsertSpawningImages com os valores corretos', () async {
    await sut.saveImages(imagesInBase64: imagesInBase64);

    verify(saveSpawningImagesSpy.insert(
      cdProposta: batch.cdProposta,
      images: imagesInBase64,
      bl: batch.txBl,
    ));
  });

  test('Deve retornar false se o InsertSpawningImages falhar', () async {
    mockSaveImagesError();
    final result = await sut.saveImages(imagesInBase64: imagesInBase64);
    expect(result, false);
  });
}
