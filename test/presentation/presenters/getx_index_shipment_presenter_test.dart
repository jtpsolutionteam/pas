import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/presentation/presenters/presenters.dart';

import '../../test.dart';

void main() {
  GetxIndexShipmentPresenter sut;
  RemoveCurrentAccountSpy removeCurrentAccount;
  IndexShipmentSpy indexShipment;

  setUp(() {
    removeCurrentAccount = RemoveCurrentAccountSpy();
    indexShipment = IndexShipmentSpy();

    sut = GetxIndexShipmentPresenter(
      removeCurrentAccount: removeCurrentAccount,
      indexShipment: indexShipment,
    );
  });

  PostExpectation mockCallIndexShipment() {
    return when(indexShipment.index());
  }

  test('Deve chamar o IndexShipment corretamente', () async {
    await sut.index(durationInSeconds: 0);
    verify(indexShipment.index());
  });

  test('Deve retornar um List<ShipmentEntity> se o IndexShipment funcionar corretamente', () async {
    List<ShipmentEntity> mock = [];

    mockIndexShipment.forEach((element) {
      mock.add(ShipmentEntity.fromMap(element));
    });

    mockCallIndexShipment().thenAnswer((_) async => mock);

    final value = await sut.index(durationInSeconds: 0);

    expect(value, mock);
  });

  test('Garantir a chamada do RemoveCurrentAccount se o IndexShipment retornar um InvalidCredentialsError', () async {
    mockCallIndexShipment().thenThrow(DomainError.invalidCredentials);

    sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/login')));

    await sut.index(durationInSeconds: 0);

    verify(removeCurrentAccount.remove());
  });

  test('Se houver qualquer erro inesperado, lançar uma exceção', () async {
    mockCallIndexShipment().thenThrow(DomainError.unexpected);

    final future = sut.index(durationInSeconds: 0);

    expect(future, throwsA(DomainError.unexpected));
  });
}
