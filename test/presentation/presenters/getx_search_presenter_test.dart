import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/presentation/presenters/presenters.dart';

import '../../mocks/mocks.dart';
import '../../test.dart';

void main() {
  String accessToken;
  String code;
  String url;
  HttpClient client;
  RemoveCurrentAccount removeCurrentAccount;
  RemoteSearchBatch remoteSearchBatch;
  LoadCurrentAccountSpy loadCurrentAccount;
  GetxSearchPresenter sut;

  PostExpectation mockRequest() {
    return when(
      client.request(
        url: anyNamed('url'),
        method: anyNamed('method'),
        headers: anyNamed('headers'),
        body: anyNamed('body'),
      ),
    );
  }

  void mockRequestData(dynamic data) => mockRequest().thenAnswer((_) async => data);

  void mockRequestError(HttpError error) => mockRequest().thenThrow(error);

  mockValidData() => mockListBatch;

  void mockLoadCurrentAccount() {
    when(loadCurrentAccount.load()).thenAnswer(
      (_) async => AccountEntity(
        id: 123,
        token: accessToken,
      ),
    );
  }

  setUp(() {
    accessToken = faker.guid.guid();
    code = faker.guid.guid();
    url = faker.internet.httpUrl();
    client = HttpClientSpy();
    loadCurrentAccount = LoadCurrentAccountSpy();
    removeCurrentAccount = RemoveCurrentAccountSpy();
    remoteSearchBatch = RemoteSearchBatch(
      httpClient: client,
      urlToBL: url,
      urlToBatch: url,
      fetchCache: loadCurrentAccount,
    );
    sut = GetxSearchPresenter(
      removeCurrentAccount: removeCurrentAccount,
      searchBatch: remoteSearchBatch,
    );

    mockRequestData(mockValidData());
    mockLoadCurrentAccount();
  });

  group('Testando o RemoteSearchBatch passando o lote como parâmetro', () {
    test('Garantir a chamada do RemoteSearchBatch com o parâmetro correto', () async {
      await sut.search(
        parameter: code,
        typeParameter: SearchBatchParams.batch,
      );

      final apiUrl = url + code;

      verify(
        client.request(
          method: 'get',
          url: apiUrl,
          headers: {
            'Authorization': 'Bearer $accessToken',
          },
        ),
      );
    });

    test('Garantir a chamada do RemoveCurrentAccount se o RemoteSearchBatch retornar um UnauthorizedError', () async {
      mockRequestError(HttpError.unauthorized);

      sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/login')));

      await sut.search(
        parameter: code,
        typeParameter: SearchBatchParams.batch,
        durationInSeconds: 0,
      );

      verify(removeCurrentAccount.remove());
    });
  });

  group('Testando o RemoteSearchBatch passando o código bl como parâmetro', () {
    test('Garantir a chamada do RemoteSearchBatch com o parâmetro correto', () async {
      await sut.search(
        parameter: code,
        typeParameter: SearchBatchParams.bl,
      );

      final apiUrl = url + code;

      verify(
        client.request(
          method: 'get',
          url: apiUrl,
          headers: {
            'Authorization': 'Bearer $accessToken',
          },
        ),
      );
    });

    test('Garantir a chamada do RemoveCurrentAccount se o RemoteSearchBatch retornar um UnauthorizedError', () async {
      mockRequestError(HttpError.unauthorized);

      sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/login')));

      await sut.search(
        parameter: code,
        typeParameter: SearchBatchParams.bl,
        durationInSeconds: 0,
      );

      verify(removeCurrentAccount.remove());
    });
  });
}
