import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/presentation/presenters/presenters.dart';

import '../../test.dart';

void main() {
  GetxBatchShipmentPresenter sut;
  InsertBatchShipmentImagesSpy insertImages;
  ListCarregamentoLote batchShipment;
  List<String> imagesInBase64;

  setUp(() {
    batchShipment = ListCarregamentoLote.fromMap(
      mockListCarregamentoLote,
    );
    insertImages = InsertBatchShipmentImagesSpy();
    imagesInBase64 = ['image', 'image', 'image', 'image'];

    sut = GetxBatchShipmentPresenter(
      batchShipment: batchShipment,
      save: insertImages,
    );
  });


  void mockCallInsertImagesError() {
    return when(insertImages.insert(
      cdProposta: anyNamed('cdProposta'),
      images: anyNamed('images'),
      txProposta: anyNamed('txProposta'),
    )).thenThrow(new Exception());
  }

  test('Garantir a chamada correta do InsertBatchShipmentImages', () async {
    final value = await sut.saveImages(imagesInBase64: imagesInBase64);

    verify(insertImages.insert(
      cdProposta: batchShipment.cdProposta,
      images: imagesInBase64,
      txProposta: batchShipment.txProposta,
    ));

    expect(value, true);
  });  

  test('Garantir o retorno de false se o InsertBatchShipmentImages falhar', () async {
    mockCallInsertImagesError();

    final value = await sut.saveImages(imagesInBase64: imagesInBase64);

    expect(value, false);
  });
}
