import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/presentation/presenters/presenters.dart';

import '../../test.dart';

void main() {
  GetxBatchImagesPresenter sut;
  LoadCurrentAccount loadCurrentAccount;
  AccountEntity fakeAccountEntity;

  PostExpectation mockLoadCurrentAccountCall() {
    return when(loadCurrentAccount.load());
  }

  void mockSuccessLoadCurrentAccount(AccountEntity account) {
    return mockLoadCurrentAccountCall().thenAnswer((_) async => account);
  }

  void mockErrorLoadCurrentAccount() {
    return mockLoadCurrentAccountCall().thenThrow(new Exception());
  }

  setUp(() {
    loadCurrentAccount = LoadCurrentAccountSpy();
    sut = GetxBatchImagesPresenter(loadCurrentAccount);
    fakeAccountEntity = AccountEntity(id: 456, token: '456');

    mockSuccessLoadCurrentAccount(fakeAccountEntity);
  });

  test('Garantir a chamada do LoadCurrentAccount', () async {
    await sut.loadAccount();
    verify(loadCurrentAccount.load());
  });

  test('Deve retornar um AccountEntity', () async {
    final account = await sut.loadAccount();
    expect(account, fakeAccountEntity);
  });

  test('Lançar um UnexpectedError se LoadCurrentAccount falhar', () async {
    mockErrorLoadCurrentAccount();
    final future = sut.loadAccount();
    expect(future, throwsA(DomainError.unexpected));
  });
}
