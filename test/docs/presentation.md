# Cobertura de testes
## Presenters
- ✔️ getx_batch_images_presenter
- ✔️ getx_batch_save_presenter
- ✔️ getx_batch_shipment_presenter
- ✔️ getx_index_shipment_presenter
- ✔️ getx_login_presenter
- ✔️ getx_search_presenter
- ✔️ getx_splash_presenter
- ✔️ getx_upload_image_shipment_presenter
- ✔️ getx_upload_image_spawning_presenter