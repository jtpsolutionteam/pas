# Cobertura de testes

## Casos de uso
### Account
- ✔️ local_load_current_account
- ✔️ local_remove_current_account
- ✔️ local_save_current_account

### Authentication
- ✔️ remote_authentication

### Batch Shipment
- ✔️ local_delete_images
- ✔️ local_index_images
- ✔️ local_insert_images

### Search Batch
- ✔️ remote_search_batch

### Shipment
- ✔️ remote_list_shipment

### Spawning Images
- ✔️ local_delete_images
- ✔️ local_index_images
- ✔️ local_insert_images

### Upload Images
- ✔️ remote_upload_images