import 'package:flutter_test/flutter_test.dart';
import 'package:pas/utils/utils.dart';

void main() {
  String imagePath;
  List<String> imagesPaths;

  setUp(() {
    imagePath = 'lib/ui/assets/images/amalog-white.png';
    imagesPaths = [imagePath, imagePath, imagePath, imagePath];
  });

  test('Deve retornar uma string em base64', () async {
    final base64 = await convertImageToBase64(image: imagePath);
    expect(base64.length, isNotNull);
  });

  test('Deve retornar uma exceção se a convertImageToBase64 falhar', () async {
    final future = convertImageToBase64(image: null);
    expect(future, throwsA(isA<Exception>()));
  });

  test('Deve retornar uma lista de strings em base64', () async {
    final base64Images = await convertImagesToBase64(imagesPaths);
    expect(base64Images.length, imagesPaths.length);
  });

  test('Deve retornar uma exceção se a convertImagesToBase64 falhar', () async {
    final future = convertImagesToBase64(null);
    expect(future, throwsA(isA<Exception>()));
  });
}
