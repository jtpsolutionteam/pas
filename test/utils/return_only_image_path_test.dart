import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:pas/utils/utils.dart';

void main() {
  test('Deve retornar a própria url se nela contiver um caminho de imagem válida', () {
    final String pathPNG = faker.internet.httpsUrl() + '/image.png';
    final testPNG = returnOnlyImagePath(pathPNG);
    expect(testPNG, pathPNG);

    final String pathJPEG = faker.internet.httpsUrl() + '/image.jpeg';
    final testJPEG = returnOnlyImagePath(pathJPEG);
    expect(testJPEG, pathJPEG);

    final String pathJPG = faker.internet.httpsUrl() + '/image.jpg';
    final testJPG = returnOnlyImagePath(pathJPG);
    expect(testJPG, pathJPG);
  });

  test('Deve retornar null se a url não tiver nenhum caminho de imagem', () {
    final String path = faker.internet.httpsUrl();
    final test = returnOnlyImagePath(path);
    expect(test, null);
  });

  test('Deve retornar uma exceção se a função falhar', () {
    expect(() => returnOnlyImagePath(null), throwsException);
  });
}
