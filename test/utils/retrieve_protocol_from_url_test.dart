import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:pas/utils/utils.dart';

void main() {
  test('Deve retornar https se a url tiver o protocolo https', () {
    final protocol = retrieveProtocolFromUrl(faker.internet.httpsUrl());
    expect(protocol, Protocol.https);
  });

  test('Deve retornar http se a url tiver o protocolo http', () {
    final protocol = retrieveProtocolFromUrl(faker.internet.httpUrl());
    expect(protocol, Protocol.http);
  });

  test('Deve retornar nulo se a url não tiver nenhum protocolo', () {
    final protocol = retrieveProtocolFromUrl('invalid_url');
    expect(protocol, null);
  });

  test('Deve retornar uma exceção se a função falhar', () {
    expect(() => retrieveProtocolFromUrl(null), throwsException);
  });
}
