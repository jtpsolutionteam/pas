import 'package:flutter_test/flutter_test.dart';
import 'package:pas/utils/utils.dart';

void main() {
  test('Deve retornar uma string vazia se não for fornecido uma data válida', () {
    var date = returnFormattedDate('sfgsgga');
    expect(date, '');
  });

  test('Deve retornar uma string formatada em forma de data', () {
    var dateTest = DateTime(2001, 10, 30, 12, 0).toString();
    var date = returnFormattedDate(dateTest);
    expect(date, '30/10/2001 às 12h00');
  });
}
