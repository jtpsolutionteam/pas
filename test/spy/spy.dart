import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/cache/cache.dart';
import 'package:pas/data/database/database.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/presentation/protocols/protocols.dart';
import 'package:pas/validation/protocols/protocols.dart';

class FetchSecureCacheStorageSpy extends Mock implements FetchSecureCacheStorage {}

class RemoveSecureCacheStorageSpy extends Mock implements RemoveSecureCacheStorage {}

class SaveSecureCacheStorageSpy extends Mock implements SaveSecureCacheStorage {}

class FlutterSecureStorageSpy extends Mock implements FlutterSecureStorage {}

class HttpClientSpy extends Mock implements HttpClient {}

class ClientSpy extends Mock implements Client {}

class LoadCurrentAccountSpy extends Mock implements LoadCurrentAccount {}

class RemoveCurrentAccountSpy extends Mock implements RemoveCurrentAccount {}

class SaveCurrentAccountSpy extends Mock implements SaveCurrentAccount {}

class AuthenticationSpy extends Mock implements Authentication {}

class FieldValidationSpy extends Mock implements FieldValidation {}

class ValidationSpy extends Mock implements Validation {}

class DbDeleteSpawningImagesSpy extends Mock implements DbDeleteSpawningImages {}

class DbIndexSpawningImagesSpy extends Mock implements DbIndexSpawningImages {}

class DbInsertSpawningImagesSpy extends Mock implements DbInsertSpawningImages {}

class InsertSpawningImagesSpy extends Mock implements InsertSpawningImages {}

class IndexSpawningImagesSpy extends Mock implements IndexSpawningImages {}

class DeleteSpawningImagesSpy extends Mock implements DeleteSpawningImages {}

class UploadImagesSpy extends Mock implements UploadImages {}

class DbDeleteBatchShipmentImagesSpy extends Mock implements DbDeleteBatchShipmentImages {}

class DbIndexBatchShipmentImagesSpy extends Mock implements DbIndexBatchShipmentImages {}

class DbInsertBatchShipmentImagesSpy extends Mock implements DbInsertBatchShipmentImages {}

class IndexShipmentSpy extends Mock implements IndexShipment {}

class InsertBatchShipmentImagesSpy extends Mock implements InsertBatchShipmentImages {}
