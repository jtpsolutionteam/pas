import 'package:faker/faker.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/infra/http/http.dart';
import 'package:test/test.dart';

import '../../test.dart';

void main() {
  HttpAdapter sut;
  ClientSpy client;
  String url;

  setUp(() {
    client = ClientSpy();
    sut = HttpAdapter(client);
    url = faker.internet.httpUrl();
  });

  group('Compartilhado entre todos', () {
    test('Lançar um ServerError se um método inválido for fornecido', () async {
      final future = sut.request(url: url, method: 'invalid_method');
      expect(future, throwsA(HttpError.serverError));
    });
  });

  group('Método: Post', () {
    PostExpectation mockRequest() {
      return when(
        client.post(
          any,
          body: anyNamed('body'),
          headers: anyNamed('headers'),
        ),
      );
    }

    void mockResponse(int statusCode, {String body = '{"any_key":"any_value"}'}) {
      mockRequest().thenAnswer((_) async => Response(body, statusCode));
    }

    void mockError() {
      mockRequest().thenThrow(Exception());
    }

    setUp(() {
      mockResponse(200);
    });

    test('Garantir a chamada do POST com os valores corretos', () async {
      await sut.request(
        url: url,
        method: 'post',
        body: {'any_key': 'any_value'},
        headers: {
          'content-type': 'application/json; charset=utf-8',
          'accept': 'application/json',
          'Authorization': 'Basic YW1hbG9nOmFtYWxvZyoyMDIw',
        },
      );

      verify(
        client.post(
          url,
          headers: {
            'content-type': 'application/json; charset=utf-8',
            'accept': 'application/json',
            'Authorization': 'Basic YW1hbG9nOmFtYWxvZyoyMDIw',
          },
          body: '{"any_key":"any_value"}',
        ),
      );
    });

    test('Garantir a chamada do POST sem um corpo da requisição', () async {
      await sut.request(url: url, method: 'post');
      verify(client.post(any, headers: anyNamed('headers')));
    });

    test('Retornar dados se o POST retornar 200', () async {
      final response = await sut.request(url: url, method: 'post');
      expect(response, {'any_key': 'any_value'});
    });

    test('Retornar nulo se o POST retornar 200 sem corpo de requisição', () async {
      mockResponse(200, body: '');
      final response = await sut.request(url: url, method: 'post');
      expect(response, null);
    });

    test('Lançar um BadRequestError se o POST retornar 400', () async {
      mockResponse(400);
      final future = sut.request(url: url, method: 'post');
      expect(future, throwsA(HttpError.badRequest));
    });

    test('Lançar um BadRequestError se o POST retornar 400', () async {
      mockResponse(400, body: '');
      final future = sut.request(url: url, method: 'post');
      expect(future, throwsA(HttpError.badRequest));
    });

    test('Lançar um Unauthorized se o POST retornar 401', () async {
      mockResponse(401);
      final future = sut.request(url: url, method: 'post');
      expect(future, throwsA(HttpError.unauthorized));
    });

    test('Lançar um ForbidenError se o POST retornar 403', () async {
      mockResponse(403);
      final future = sut.request(url: url, method: 'post');
      expect(future, throwsA(HttpError.forbbiden));
    });

    test('Lançar um NotFoundError se o POST retornar 404', () async {
      mockResponse(404);
      final future = sut.request(url: url, method: 'post');
      expect(future, throwsA(HttpError.notFound));
    });

    test('Lançar um ServerError se o POST retornar 500', () async {
      mockResponse(500);
      final future = sut.request(url: url, method: 'post');
      expect(future, throwsA(HttpError.serverError));
    });

    test('Lançar um ServerError se o POST falhar', () async {
      mockError();
      final future = sut.request(url: url, method: 'post');
      expect(future, throwsA(HttpError.serverError));
    });
  });

  group('Método: Get', () {
    PostExpectation mockRequest() {
      return when(client.get(any, headers: anyNamed('headers')));
    }

    void mockResponse(int statusCode, {String body = '{"any_key":"any_value"}'}) {
      mockRequest().thenAnswer((_) async => Response(body, statusCode));
    }

    void mockError() => mockRequest().thenThrow(Exception());

    setUp(() {
      mockResponse(200);
    });

    test('Garantir a chamada do GET com os valores corretos', () async {
      await sut.request(url: url, method: 'get');

      verify(client.get(
        url,
        headers: {
          'content-type': 'application/json; charset=utf-8',
          'accept': 'application/json',
        },
      ));

      await sut.request(url: url, method: 'get', headers: {'any_header': 'any_value'});

      verify(client.get(
        url,
        headers: {
          'content-type': 'application/json; charset=utf-8',
          'accept': 'application/json',
          'any_header': 'any_value',
        },
      ));
    });

    test('Retornar dados se o GET responder com 200', () async {
      final response = await sut.request(url: url, method: 'get');
      expect(response, {'any_key': 'any_value'});
    });

    test('Retornar nulo se o GET responder com 200 sem corpo de requisição', () async {
      mockResponse(200, body: '');
      final response = await sut.request(url: url, method: 'get');
      expect(response, null);
    });

    test('Retornar nulo se o GET responder com 204', () async {
      mockResponse(204, body: '');
      final response = await sut.request(url: url, method: 'get');
      expect(response, null);
    });

    test('Retornar nulo se o GET responder com 200 sem dados', () async {
      mockResponse(204);
      final response = await sut.request(url: url, method: 'get');
      expect(response, null);
    });

    test('Lançar um BadRequestError se o GET retornar 400', () async {
      mockResponse(400, body: '');
      final future = sut.request(url: url, method: 'get');
      expect(future, throwsA(HttpError.badRequest));
    });

    test('Lançar um BadRequestError se o GET retornar 400', () async {
      mockResponse(400);
      final future = sut.request(url: url, method: 'get');
      expect(future, throwsA(HttpError.badRequest));
    });

    test('Lançar um UnauthorizedError se o GET retornar 401', () async {
      mockResponse(401);
      final future = sut.request(url: url, method: 'get');
      expect(future, throwsA(HttpError.unauthorized));
    });

    test('Lançar um ForbiddenError se o GET retornar 403', () async {
      mockResponse(403);
      final future = sut.request(url: url, method: 'get');
      expect(future, throwsA(HttpError.forbbiden));
    });

    test('Lançar um NotFoundError se o GET retornar 404', () async {
      mockResponse(404);
      final future = sut.request(url: url, method: 'get');
      expect(future, throwsA(HttpError.notFound));
    });

    test('Lançar um ServerError se o GET retornar 500', () async {
      mockResponse(500);
      final future = sut.request(url: url, method: 'get');
      expect(future, throwsA(HttpError.serverError));
    });

    test('Lançar um ServerError se o GET falhar', () async {
      mockError();
      final future = sut.request(url: url, method: 'get');
      expect(future, throwsA(HttpError.serverError));
    });
  });

  group('Método: Put', () {});

  group('Método: Delete', () {});
}
