import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/infra/cache/cache.dart';

import '../../test.dart';

void main() {
  FlutterSecureStorageSpy secureStorage;
  LocalStorageAdapter sut;
  String value;
  String key;

  setUp(() {
    secureStorage = FlutterSecureStorageSpy();
    sut = LocalStorageAdapter(secureStorage: secureStorage);
    key = faker.lorem.word();
    value = faker.guid.guid();
  });

  group('Método: SaveSecure', () {
    void mockSaveSecureError() {
      when(secureStorage.write(key: anyNamed('key'), value: anyNamed('value'))).thenThrow(Exception());
    }

    test('Garantir a chamada do SaveSecure com os valores corretos', () async {
      await sut.saveSecure(key: key, value: value);
      verify(secureStorage.write(key: key, value: value));
    });

    test('Lançar uma exceção se o SaveSecure falhar', () async {
      mockSaveSecureError();
      final future = sut.saveSecure(key: key, value: value);
      expect(future, throwsA(isA<Exception>()));
    });
  });

  group('Método: RemoveSecure', () {
    void mockRemoveSecureError() {
      when(secureStorage.deleteAll()).thenThrow(Exception());
    }

    test('Garantir a chamada do RemoveSecure', () async {
      await sut.removeSecure();
      verify(secureStorage.deleteAll());
    });

    test('Lançar uma exceção se o RemoveSecure falhar', () async {
      mockRemoveSecureError();
      final future = sut.removeSecure();
      expect(future, throwsA(isA<Exception>()));
    });
  });

  group('Método: FetchSecure', () {
    PostExpectation mockFetchSecureCall() {
      return when(secureStorage.read(key: anyNamed('key')));
    }

    void mockFetchSecure() {
      mockFetchSecureCall().thenAnswer((_) async => value);
    }

    void mockFetchSecureError() {
      mockFetchSecureCall().thenThrow(Exception());
    }

    setUp(() {
      mockFetchSecure();
    });

    test('Garantir a chamada do FetchSecure com os valores corretos', () async {
      await sut.fetchSecure(key);
      verify(secureStorage.read(key: key));
    });

    test('Retornar o valor correto', () async {
      final fetchedValue = await sut.fetchSecure(key);
      expect(fetchedValue, value);
    });

    test('Lançar uma exceção se o FetchSecure falhar', () async {
      mockFetchSecureError();
      final future = sut.fetchSecure(key);
      expect(future, throwsA(isA<Exception>()));
    });
  });
}
