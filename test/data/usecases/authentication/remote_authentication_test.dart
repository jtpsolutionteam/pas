import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

import '../../../test.dart';

void main() {
  HttpClientSpy httpClient;
  String url;
  RemoteAuthentication sut;
  AuthenticationParams params;
  String email;
  String password;

  PostExpectation mockRequest() {
    return when(httpClient.request(
      url: anyNamed('url'),
      method: anyNamed('method'),
      headers: anyNamed('headers'),
      body: anyNamed('body'),
    ));
  }

  Map mockValidData() {
    return {
      'access_token': faker.guid.guid(),
      'cdUsuario': 1,
    };
  }

  void mockRequestData(Map data) {
    mockRequest().thenAnswer((_) async => data);
  }

  void mockRequestError(HttpError error) {
    mockRequest().thenThrow(error);
  }

  String urlWithQueryparams(String url, String email, String password) {
    return url + 'username=$email' + '&password=$password' + '&grant_type=password';
  }

  setUp(() {
    httpClient = HttpClientSpy();
    url = faker.internet.httpUrl();
    email = faker.internet.email();
    password = faker.internet.password();
    sut = RemoteAuthentication(httpClient: httpClient, url: url);
    params = AuthenticationParams(email: email, password: password);

    mockRequestData(mockValidData());
  });

  test('Chamar o HttpClient com os valores corretos', () async {
    await sut.auth(params);
    final apiUrl = urlWithQueryparams(url, email, password);

    verify(httpClient.request(
      url: apiUrl,
      method: 'post',
      headers: {'Authorization': 'Basic YW1hbG9nOmFtYWxvZyoyMDIw'},
    ));
  });

  test('Lançar um InvalidCredentialsError se HttpClient retornar 400', () async {
    mockRequestError(HttpError.badRequest);
    final future = sut.auth(params);
    expect(future, throwsA(DomainError.invalidCredentials));
  });

  test('Lançar um InvalidCredentialsError se HttpClient retornar 401', () async {
    mockRequestError(HttpError.unauthorized);
    final future = sut.auth(params);
    expect(future, throwsA(DomainError.invalidCredentials));
  });

  test('Lançar um UnexpectedError se HttpClient retornar 404', () async {
    mockRequestError(HttpError.notFound);
    final future = sut.auth(params);
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Lançar um UnexpectedError se HttpClient retornar 500', () async {
    mockRequestError(HttpError.serverError);
    final future = sut.auth(params);
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Retornar um Account se HttpClient retornar 200', () async {
    final validData = mockValidData();
    mockRequestData(validData);

    final account = await sut.auth(params);
    expect(account.token, validData['access_token']);
    expect(account.id, validData['cdUsuario']);
  });

  test('Lançar um UnexpectedError se HttpClient retornar 200 com valores inválidos', () async {
    mockRequestData({
      'invalid_key': 'invalid_value',
    });

    final future = sut.auth(params);
    expect(future, throwsA(DomainError.unexpected));
  });
}
