import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/data/usecases/upload_images/upload_images.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../../../test.dart';

void main() {
  HttpClient httpClient;
  RemoteUploadImages sut;
  LoadCurrentAccountSpy loadCurrentAccount;
  String url;
  String accessToken;
  int idUser;
  int cdProposta;
  var images;

  PostExpectation mockRequest() {
    return when(httpClient.request(
      url: anyNamed('url'),
      method: anyNamed('method'),
      headers: anyNamed('headers'),
      body: anyNamed('body'),
    ));
  }

  void mockRequestData(Map data) {
    mockRequest().thenAnswer((_) async => data);
  }

  void mockRequestError(HttpError error) {
    mockRequest().thenThrow(error);
  }

  void mockLoadAccountSuccess() {
    when(loadCurrentAccount.load()).thenAnswer(
      (_) async => AccountEntity(token: accessToken, id: idUser),
    );
  }

  setUp(() {
    httpClient = HttpClientSpy();
    loadCurrentAccount = LoadCurrentAccountSpy();
    url = faker.internet.httpsUrl();
    accessToken = faker.guid.guid();
    cdProposta = 123456;
    idUser = 123;
    images = ['string_base64', 'string_base64'];
    sut = RemoteUploadImages(
      fetchCache: loadCurrentAccount,
      httpClient: httpClient,
      url: url,
    );

    mockRequestData(null);
    mockLoadAccountSuccess();
  });

  test('Garantir a chamada do HttpClient com os valores corretos', () async {
    List<ListFoto> imagesPaths = [];

    images.forEach((e) => imagesPaths.add(ListFoto(imagePath: e)));

    var body = RemoteUploadImagesApiParams(
      cdProposta: cdProposta,
      listFotos: imagesPaths,
      cdUsuario: idUser,
    ).toMap();

    await sut.upload(imagesInBase64: images, cdProposta: cdProposta);

    verify(
      httpClient.request(
        url: url,
        method: 'post',
        headers: {'Authorization': 'Bearer $accessToken'},
        body: body,
      ),
    );
  });

  test('Garantir a chamada do LoadCurrentAccount', () async {
    await sut.upload(imagesInBase64: images, cdProposta: cdProposta);
    verify(loadCurrentAccount.load()).called(1);
  });

  test('Lançar um UnexpectedError se o HttpClient retornar 400', () {
    mockRequestError(HttpError.badRequest);
    final future = sut.upload(imagesInBase64: images, cdProposta: cdProposta);
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Lançar um InvalidCredentialsError se o HttpClient retornar 401', () {
    mockRequestError(HttpError.unauthorized);
    final future = sut.upload(imagesInBase64: images, cdProposta: cdProposta);
    expect(future, throwsA(DomainError.invalidCredentials));
  });

  test('Lançar um UnexpectedError se o HttpClient retornar 500  ', () {
    mockRequestError(HttpError.serverError);
    final future = sut.upload(imagesInBase64: images, cdProposta: cdProposta);
    expect(future, throwsA(DomainError.unexpected));
  });
}
