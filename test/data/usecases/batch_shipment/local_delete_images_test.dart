import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../../../test.dart';

void main() {
  LocalDeleteBatchShipmentImages sut;
  DbDeleteBatchShipmentImagesSpy deleteImages;

  setUp(() {
    deleteImages = DbDeleteBatchShipmentImagesSpy();
    sut = LocalDeleteBatchShipmentImages(deleteImages);
  });

  test('Garantir a chamada do DeleteImages com os valores corretos', () async {
    await sut.delete(123);
    verify(deleteImages.delete(123));
  });

  test('Lançar um UnexpectedError se o DeleteImages falhar', () async {
    when(deleteImages.delete(any)).thenThrow(Exception());

    final future = sut.delete(789);
    expect(future, throwsA(DomainError.unexpected));
  });
}
