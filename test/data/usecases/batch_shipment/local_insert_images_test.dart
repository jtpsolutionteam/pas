import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../../../test.dart';

void main() {
  LocalInsertBatchShipmentImages sut;
  DbInsertBatchShipmentImagesSpy insertImages;
  int cdProposta;
  String txProposta;
  List<String> images;

  setUp(() {
    insertImages = DbInsertBatchShipmentImagesSpy();
    sut = LocalInsertBatchShipmentImages(insertImages);

    cdProposta = 123;
    images = ['img', 'img', 'img'];
    txProposta = 'assadaf';
  });

  test('Garantir a chamada do InsertImages com os valores corretos', () async {
    await sut.insert(
      cdProposta: cdProposta,
      images: images,
      txProposta: txProposta,
    );

    verify(insertImages.insert(
      cdProposta: cdProposta,
      images: images,
      txProposta: txProposta,
    ));
  });

  test('Lançar um UnexpectedError se o InsertImages falhar', () async {
    when(insertImages.insert(
      cdProposta: anyNamed('cdProposta'),
      images: anyNamed('images'),
      txProposta: anyNamed('txProposta'),
    )).thenThrow(Exception());

    final future = sut.insert(
      cdProposta: cdProposta,
      images: images,
      txProposta: txProposta,
    );

    expect(future, throwsA(DomainError.unexpected));
  });
}
