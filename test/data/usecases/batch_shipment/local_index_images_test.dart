import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../../../test.dart';

void main() {
  LocalIndexBatchShipmentImages sut;
  DbIndexBatchShipmentImagesSpy listImages;

  setUp(() {
    listImages = DbIndexBatchShipmentImagesSpy();
    sut = LocalIndexBatchShipmentImages(listImages);
  });

  test('Chamar o ListImages corretamente', () async {
    await sut.index();
    verify(listImages.index());
  });

  test('Lançar um UnexpectedError se o ListImages falhar', () async {
    when(listImages.index()).thenThrow(Exception());

    final future = sut.index();
    expect(future, throwsA(DomainError.unexpected));
  });
}
