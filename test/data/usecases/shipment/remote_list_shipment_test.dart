import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../../../test.dart';

void main() {
  String url;
  LoadCurrentAccountSpy loadCurrentAccountSpy;
  HttpClientSpy httpClientSpy;
  AccountEntity account;
  RemoteIndexShipment sut;

  PostExpectation mockLoadCurrentAccountCall() {
    return when(loadCurrentAccountSpy.load());
  }

  void mockSuccessLoadCurrentAccount() {
    return mockLoadCurrentAccountCall().thenAnswer((_) async => account);
  }

  void mockErrorLoadCurrentAccount() {
    return mockLoadCurrentAccountCall().thenThrow(new Exception());
  }

  PostExpectation mockRequest() {
    return when(httpClientSpy.request(
      url: anyNamed('url'),
      method: anyNamed('method'),
      headers: anyNamed('headers'),
      body: anyNamed('body'),
    ));
  }

  void mockRequestData(dynamic data) => mockRequest().thenAnswer((_) async => data);

  void mockRequestError(HttpError error) => mockRequest().thenThrow(error);

  setUp(() {
    url = faker.internet.httpsUrl();
    account = AccountEntity(id: 123, token: faker.guid.guid());
    loadCurrentAccountSpy = LoadCurrentAccountSpy();
    httpClientSpy = HttpClientSpy();
    sut = RemoteIndexShipment(
      url: url,
      fetchCache: loadCurrentAccountSpy,
      httpClient: httpClientSpy,
    );

    mockSuccessLoadCurrentAccount();
    mockRequestData(mockIndexShipment);
  });

  test('Garantir a chamada do LoadCurrentAccount', () async {
    await sut.index();
    verify(loadCurrentAccountSpy.load()).called(1);
  });

  test('Garantir que se o LoadCurrenteAccount falhar, um UnexpectedError será lançado', () async {
    mockErrorLoadCurrentAccount();
    var future = sut.index();
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Garantir a chamada do HttpClient com os valores corretos', () async {
    await sut.index();
    var token = account.token;

    verify(httpClientSpy.request(
      url: url,
      method: 'get',
      headers: {'Authorization': 'Bearer $token'},
    ));
  });

  test('Garantir que se o HttpClient falhar, um UnexpectedError será lançado', () async {
    mockRequestError(HttpError.badRequest);
    var future = sut.index();
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Garantir que se o HttpClient retornar 401, um UnexpectedError será lançado', () async {
    mockRequestError(HttpError.unauthorized);
    var future = sut.index();
    expect(future, throwsA(DomainError.invalidCredentials));
  });

  test('Garantir que seja retornado uma indexa de carregamentos caso o HttpClient retorne 200', () async {
    final index = await sut.index();
    expect(index.length, mockIndexShipment.length);
    expect(index[0].txLote, mockIndexShipment[0]['txLote']);
  });
}
