import 'package:mockito/mockito.dart';
import 'package:pas/data/cache/cache.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:test/test.dart';

import '../../../test.dart';

void main() {
  RemoveSecureCacheStorage removeSecureCacheStorage;
  LocalRemoveCurrentAccount sut;

  setUp(() {
    removeSecureCacheStorage = RemoveSecureCacheStorageSpy();
    sut = LocalRemoveCurrentAccount(
      removeSecureCacheStorage: removeSecureCacheStorage,
    );
  });

  test('Garatir que RemoveSecureCacheStorage seja chamado', () async {
    await sut.remove();
    verify(removeSecureCacheStorage.removeSecure());
  });

  test('Lançar uma exceção do tipo UnexpectedError se RemoveSecureCacheStorage falhar', () async {
    when(removeSecureCacheStorage.removeSecure()).thenThrow(Exception());
    final future = sut.remove();
    expect(future, throwsA(DomainError.unexpected));
  });
}
