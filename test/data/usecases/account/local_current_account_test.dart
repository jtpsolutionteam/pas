import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../../../test.dart';

void main() {
  FetchSecureCacheStorageSpy fetchSecureCacheStorage;
  LocalLoadCurrentAccount sut;
  String token;
  String idUser;

  PostExpectation mockFetchSecureToken() => when(fetchSecureCacheStorage.fetchSecure('token'));

  PostExpectation mockFetchSecureId() => when(fetchSecureCacheStorage.fetchSecure('id'));

  void mockFetchSecureError() {
    when(fetchSecureCacheStorage.fetchSecure(any)).thenThrow(Exception);
  }

  void mockFetchSecureCalls() {
    mockFetchSecureToken().thenAnswer((_) async => token);
    mockFetchSecureId().thenAnswer((_) async => idUser);
  }

  setUp(() {
    fetchSecureCacheStorage = FetchSecureCacheStorageSpy();
    sut = LocalLoadCurrentAccount(fetchSecureCacheStorage);
    token = faker.guid.guid();
    idUser = '123';

    mockFetchSecureCalls();
  });

  test('Garatir que FetchSecureCacheStorage seja chamado', () async {
    await sut.load();
    verify(fetchSecureCacheStorage.fetchSecure('token'));
  });

  test('Garantir que um AccountEntity seja retornado', () async {
    final account = await sut.load();
    expect(account, AccountEntity(token: token, id: int.parse(idUser)));
  });

  test('Lançar uma exceção do tipo UnexpectedError se FetchSecureCacheStorage falhar', () async {
    mockFetchSecureError();
    final future = sut.load();
    expect(future, throwsA(DomainError.unexpected));
  });
}
