import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../../../test.dart';

void main() {
  LocalSaveCurrentAccount sut;
  SaveSecureCacheStorageSpy saveSecureCacheStorage;
  AccountEntity account;

  setUp(() {
    saveSecureCacheStorage = SaveSecureCacheStorageSpy();
    sut = LocalSaveCurrentAccount(
      saveSecureCacheStorage: saveSecureCacheStorage,
    );
    account = AccountEntity(
      token: faker.guid.guid(),
      id: 123,
    );
  });

  void mockError() {
    when(saveSecureCacheStorage.saveSecure(
      key: anyNamed('key'),
      value: anyNamed('value'),
    )).thenThrow(Exception());
  }

  test('Garantir que o SaveSecureCacheStorage seja chamado com os valores corretos', () async {
    await sut.save(account);
    verify(
      saveSecureCacheStorage.saveSecure(key: 'token', value: account.token),
    );
    verify(
      saveSecureCacheStorage.saveSecure(key: 'id', value: account.id.toString()),
    );
  });

  test('Lançar uma exceção do tipo UnexpectedError se SaveSecureCacheStorage falhar', () async {
    mockError();
    final future = sut.save(account);
    expect(future, throwsA(DomainError.unexpected));
  });
}
