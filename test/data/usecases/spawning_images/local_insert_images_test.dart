import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../../../test.dart';

void main() {
  LocalInsertSpawningImages sut;
  DbInsertSpawningImagesSpy insertImages;
  int cdProposta;
  String bl;
  List<String> images;

  setUp(() {
    insertImages = DbInsertSpawningImagesSpy();
    sut = LocalInsertSpawningImages(insertImages);

    cdProposta = 123;
    bl = "AMA5456465";
    images = ['', '', ''];
  });

  test('Garantir a chamada do InsertImages com os valores corretos', () async {
    await sut.insert(images: images, cdProposta: cdProposta, bl: bl);

    verify(insertImages.insert(cdProposta: cdProposta, images: images, bl: bl));
  });

  test('Lançar um UnexpectedError se o InsertImages falhar', () async {
    when(insertImages.insert(
      cdProposta: anyNamed('cdProposta'),
      images: anyNamed('images'),
      bl: anyNamed('bl'),
    )).thenThrow(Exception());

    final future = sut.insert(cdProposta: cdProposta, images: images, bl: bl);

    expect(future, throwsA(DomainError.unexpected));
  });
}
