import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../../../test.dart';

void main() {
  LocalDeleteSpawningImages sut;
  DbDeleteSpawningImagesSpy deleteImages;
  int id;

  setUp(() {
    id = 65898;
    deleteImages = DbDeleteSpawningImagesSpy();
    sut = LocalDeleteSpawningImages(deleteImages);
  });

  test('Garantir a chamada do DeleteImages com os valores corretos', () async {
    await sut.delete(id);
    verify(deleteImages.delete(id));
  });

  test('Lançar um UnexpectedError se o DeleteImages falhar', () async {
    when(deleteImages.delete(any)).thenThrow(Exception());

    final future = sut.delete(id);
    expect(future, throwsA(DomainError.unexpected));
  });
}
