import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

import '../../../test.dart';

void main() {
  String accessToken;
  String url;
  String code;
  HttpClient httpClient;
  RemoteSearchBatch sut;
  LoadCurrentAccountSpy loadCurrentAccount;

  PostExpectation mockRequest() {
    return when(httpClient.request(
      url: anyNamed('url'),
      method: anyNamed('method'),
      headers: anyNamed('headers'),
      body: anyNamed('body'),
    ));
  }

  void mockRequestData(dynamic data) => mockRequest().thenAnswer((_) async => data);

  void mockRequestError(HttpError error) => mockRequest().thenThrow(error);

  mockValidData() {
    return mockListBatch;
  }

  void mockLoadCurrentAccount() {
    when(loadCurrentAccount.load()).thenAnswer(
      (_) async => AccountEntity(
        id: 123,
        token: accessToken,
      ),
    );
  }

  setUp(() {
    code = faker.lorem.word();
    accessToken = faker.guid.guid();
    url = faker.internet.httpsUrl();
    httpClient = HttpClientSpy();
    loadCurrentAccount = LoadCurrentAccountSpy();

    sut = RemoteSearchBatch(
      httpClient: httpClient,
      urlToBL: url,
      urlToBatch: url,
      fetchCache: loadCurrentAccount,
    );

    mockRequestData(mockValidData());
    mockLoadCurrentAccount();
  });

  test('Garantir a chamada do HttpClient com os valores corretos', () async {
    await sut.search(code: code, params: SearchBatchParams.batch);

    verify(httpClient.request(
      url: url + code,
      method: 'get',
      headers: {'Authorization': 'Bearer $accessToken'},
    ));
  });

  test('Garantindo a chamada do LoadCurrentAccount', () async {
    await sut.search(code: code, params: SearchBatchParams.batch);
    verify(loadCurrentAccount.load()).called(1);
  });

  test('Garantir o retorno de uma lista de BatchEntity se o HttpClient retornar 200', () async {
    final validData = mockValidData();
    final batch = await sut.search(code: code, params: SearchBatchParams.batch);
    expect(batch[0].txBl, validData[0]['txBl']);
  });

  test('Lançar um UnexpectedError se o HttpClient retornar 400', () async {
    mockRequestError(HttpError.badRequest);
    final future = sut.search(code: code, params: SearchBatchParams.batch);
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Lançar um InvalidCredentialsError se o HttpClient retornar 401', () async {
    mockRequestError(HttpError.unauthorized);
    final future = sut.search(code: code, params: SearchBatchParams.batch);
    expect(future, throwsA(DomainError.invalidCredentials));
  });

  test('Lançar um UnexpectedError se o HttpClient retornar 404', () async {
    mockRequestError(HttpError.notFound);
    final future = sut.search(code: code, params: SearchBatchParams.batch);
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Lançar um UnexpectedError se o HttpClient retornar 500', () async {
    mockRequestError(HttpError.serverError);
    final future = sut.search(code: code, params: SearchBatchParams.batch);
    expect(future, throwsA(DomainError.unexpected));
  });
}
