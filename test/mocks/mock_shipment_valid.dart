var mockListCarregamentoLote = {
  "cdProposta": 6108,
  "cdCarregamento": 1,
  "txProposta": "AMA06108/20",
  "tabUsuarioObj": {"cdUsuario": 160, "txApelido": "ROGER"},
  "tabClienteObj": {"cdCliente": 2, "txCliente": "MSL", "txCnpj": "06101230000123"},
  "txLote": "754051",
  "vlAltura": 1.0,
  "vlLargura": 1.0,
  "vlPesoExtratoDesova": 607.0,
  "vlPesoBruto": 605.0,
  "vlM3Pack": 1.53,
  "vlComprimento": 1.53,
  "vlQtdeVolume": 57,
  "txVolume": "DESOVADOS  57 CAIXA DE PAPELAO",
  "vlCifReal": 7916.47
};

var mockShipment = {
  "cdCarregamento": 1,
  "dtCriacao": "22/01/2021 19:09:18",
  "tabUsuarioCriacaoObj": {
    "cdUsuario": 1,
    "txApelido": "ADM",
  },
  "dtPrevisaoCarregamento": "23/01/2021",
  "dtCarregamento": null,
  "tabVeiculoObj": {
    "cdVeiculo": 1,
    "txVeiculo": "TORO",
    "txPlaca": "ABC-5342",
  },
  "tabMotoristaObj": {
    "cdMotorista": 1,
    "txMotorista": "SERGIO PERALTA",
    "txCpf": "04746455767",
    "txCnh": "12345678",
  },
  "tabStatusCarregamentoObj": {
    "cdStatus": 1,
    "txStatus": "Previsão em aberto",
  },
  "vlTotalM3": 1.63,
  "vlTotalPeso": 633.0,
  "vlTotalCifReal": 11771.81,
  "txLote": null,
  "listCarregamentoLotes": [
    mockListCarregamentoLote,
    {
      "cdProposta": 12644,
      "cdCarregamento": 1,
      "txProposta": "AMA12644/20",
      "tabUsuarioObj": {"cdUsuario": 240, "txApelido": "Bruno"},
      "tabClienteObj": {"cdCliente": 35, "txCliente": "SINOSTAR AGENCIADORA DE CARGAS E LOGISTICA EIRELI", "txCnpj": "14594398000146"},
      "txLote": "803519",
      "vlAltura": 1.0,
      "vlLargura": 1.0,
      "vlPesoExtratoDesova": 28.0,
      "vlPesoBruto": 28.0,
      "vlM3Pack": 0.1,
      "vlComprimento": 0.1,
      "vlQtdeVolume": 1,
      "txVolume": "DESOVADOS  1 TAMBOR DE PAPELAO",
      "vlCifReal": 3855.34
    }
  ]
};

var mockIndexShipment = [
  mockShipment,
  mockShipment,
  mockShipment,
  mockShipment,
  mockShipment,
  mockShipment,
];
