var mockBatch = {
  "cdProposta": 10365,
  "txContainer": "EGSU501621-7",
  "tabClienteObj": {
    "cdCliente": 13,
    "txCliente": "SOUTH CARGO DO BRASIL ",
    "txCnpj": "22863181000112",
  },
  "dtProposta": "06/11/2020 16:14:39",
  "txProposta": "AMA10365/20",
  "tabStatusObj": {
    "cdStatus": 12,
    "txStatus": "DTA Concluída!",
    "txTipoStatus": "DTA",
  },
  "tabTipoCarregamentoObj": {
    "cdTipoCarregamento": 1,
    "txTipoCarregamento": "DTA Importação",
  },
  "tabDestinoObj": {
    "cdDestino": 15,
    "cdEmpresa": 2,
    "txDestino": "Varginha - MG",
    "vlDtaImportacao": 159.3,
    "vlExportacao": 146.3,
    "ckRotaAmalog": 1,
    "ckAtivo": 1,
    "cdArmazem": null,
    "cdCodigoIbge": 3170701,
  },
  "txLote": "809522",
  "txImportador": "DISTRIBUIDORA AMARAL LTDA",
  "txCnpjImportador": "21.759.758/0001-88",
  "txBl": "AMIGL200515149A",
  "listPropostasDoctos": [
    {
      "cdPropostaDocumento": 54383,
      "cdProposta": 10365,
      "txNomeArquivo": "809522.zip",
      "txUrl": "/proposta/download/10365?txNomeArquivo=809522.zip",
      "cdClassificacao": 35,
      "dtCriacao": "30/12/2020 14:30:32",
      "cdUsuarioCriacao": 101,
    }
  ]
};

var mockListBatch = [mockBatch, mockBatch, mockBatch];
