enum DomainError {
  unexpected,
  invalidCredentials,
  notFound,
  none,
}

extension DomainErrorExtension on DomainError {
  String get description {
    switch (this) {
      case DomainError.invalidCredentials:
        return 'Credenciais inválidas!';
      case DomainError.unexpected:
        return 'Algo inesperado aconteceu! Tente novamente em breve.';
      case DomainError.notFound:
        return 'Nada foi encontrado!';
      case DomainError.none:
        return 'Nenhum erro foi apresentado.';
      default:
        return '';
    }
  }
}
