import 'dart:convert';

import 'package:meta/meta.dart';

class SpawningModel {
  SpawningModel({
    @required this.cdProposta,
    @required this.dateCreate,
    @required this.bl,
    @required this.id,
  });

  final int cdProposta;
  final String dateCreate;
  final String bl;
  final String id;

  factory SpawningModel.fromJson(String str) => SpawningModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory SpawningModel.fromMap(Map<String, dynamic> json) => SpawningModel(
        cdProposta: json["cdProposta"],
        dateCreate: json["dateCreate"],
        bl: json["bl"],
        id: json["id"],
      );

  Map<String, dynamic> toMap() => {
        "cdProposta": cdProposta,
        "dateCreate": dateCreate,
        "bl": bl,
        "id": id,
      };
}

class SpawningImageModel {
  SpawningImageModel({
    @required this.idSpawning,
    @required this.imageInBase64,
  });

  final int idSpawning;
  final String imageInBase64;

  factory SpawningImageModel.fromJson(String str) => SpawningImageModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory SpawningImageModel.fromMap(Map<String, dynamic> json) => SpawningImageModel(
        idSpawning: json["idSpawning"],
        imageInBase64: json["imageInBase64"],
      );

  Map<String, dynamic> toMap() => {
        "idSpawning": idSpawning,
        "imageInBase64": imageInBase64,
      };
}

class SpawningWithImagesModel {
  final int cdProposta;
  final int id;
  final String dateCreate;
  final String bl;
  final List<String> imagesInBase64;

  SpawningWithImagesModel({
    @required this.cdProposta,
    @required this.id,
    @required this.dateCreate,
    @required this.bl,
    @required this.imagesInBase64,
  });
}
