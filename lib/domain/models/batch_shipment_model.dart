import 'dart:convert';

import 'package:meta/meta.dart';

class BatchShipmentModel {
  BatchShipmentModel({
    @required this.id,
    @required this.dateCreate,
    @required this.cdProposta,
    @required this.txProposta,
  });

  final int id;
  final String dateCreate;
  final int cdProposta;
  final String txProposta;

  factory BatchShipmentModel.fromJson(String str) => BatchShipmentModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BatchShipmentModel.fromMap(Map<String, dynamic> json) => BatchShipmentModel(
        id: json["id"],
        dateCreate: json["dateCreate"],
        cdProposta: json["cdProposta"],
        txProposta: json["txProposta"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "dateCreate": dateCreate,
        "cdProposta": cdProposta,
        "txProposta": txProposta,
      };
}

class BatchShipmentImageModel {
  BatchShipmentImageModel({
    @required this.idBatchShipment,
    @required this.imageInBase64,
  });

  final int idBatchShipment;
  final String imageInBase64;

  factory BatchShipmentImageModel.fromJson(String str) => BatchShipmentImageModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BatchShipmentImageModel.fromMap(Map<String, dynamic> json) => BatchShipmentImageModel(
        idBatchShipment: json["idBatchShipment"],
        imageInBase64: json["imageInBase64"],
      );

  Map<String, dynamic> toMap() => {
        "idBatchShipment": idBatchShipment,
        "imageInBase64": imageInBase64,
      };
}

class BatchShipmentWithImagesModel {
  BatchShipmentWithImagesModel({
    @required this.id,
    @required this.dateCreate,
    @required this.cdProposta,
    @required this.txProposta,
    @required this.imagesInBase64,
  });

  final int id;
  final String dateCreate;
  final String txProposta;
  final int cdProposta;
  final List<String> imagesInBase64;

  factory BatchShipmentWithImagesModel.fromJson(String str) => BatchShipmentWithImagesModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BatchShipmentWithImagesModel.fromMap(Map<String, dynamic> json) => BatchShipmentWithImagesModel(
        id: json["id"],
        dateCreate: json["dateCreate"],
        cdProposta: json["cdProposta"],
        txProposta: json["txProposta"],
        imagesInBase64: List<String>.from(json["imagesInBase64"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "dateCreate": dateCreate,
        "cdProposta": cdProposta,
        "txProposta": txProposta,
        "imagesInBase64": List<dynamic>.from(imagesInBase64.map((x) => x)),
      };
}
