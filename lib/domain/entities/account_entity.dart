import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class AccountEntity extends Equatable{  
  final String token;
  final int id;

  @override
  List get props => [token, id];

  AccountEntity({    
    @required this.token,
    @required this.id,
  });
}
