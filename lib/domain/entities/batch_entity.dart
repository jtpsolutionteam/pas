import 'dart:convert';

import 'package:meta/meta.dart';

import 'shared/shared.dart';

class BatchEntity {
  BatchEntity({
    @required this.cdProposta,
    @required this.tabClienteObj,
    @required this.dtProposta,
    @required this.txProposta,
    @required this.tabStatusObj,
    @required this.tabTipoCarregamentoObj,
    @required this.tabDestinoObj,
    @required this.txLote,
    @required this.txImportador,
    @required this.txCnpjImportador,
    @required this.txBl,
    @required this.txContainer,
    @required this.listPropostasDoctos,
  });

  final int cdProposta;
  final TabClienteObj tabClienteObj;
  final String dtProposta;
  final String txProposta;
  final TabStatusObj tabStatusObj;
  final TabTipoCarregamentoObj tabTipoCarregamentoObj;
  final TabDestinoObj tabDestinoObj;
  final String txLote;
  final String txImportador;
  final String txCnpjImportador;
  final String txBl;
  final String txContainer;
  final List<ListPropostasDocto> listPropostasDoctos;

  factory BatchEntity.fromJson(String str) => BatchEntity.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BatchEntity.fromMap(Map<String, dynamic> json) => BatchEntity(
        cdProposta: json["cdProposta"],
        tabClienteObj: TabClienteObj.fromMap(json["tabClienteObj"]),
        dtProposta: json["dtProposta"],
        txProposta: json["txProposta"],
        tabStatusObj: TabStatusObj.fromMap(json["tabStatusObj"]),
        tabTipoCarregamentoObj: TabTipoCarregamentoObj.fromMap(json["tabTipoCarregamentoObj"]),
        tabDestinoObj: TabDestinoObj.fromMap(json["tabDestinoObj"]),
        txLote: json["txLote"],
        txImportador: json["txImportador"],
        txCnpjImportador: json["txCnpjImportador"],
        txBl: json["txBl"],
        txContainer: json["txContainer"],
        listPropostasDoctos: List<ListPropostasDocto>.from(json["listPropostasDoctos"].map((x) => ListPropostasDocto.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "cdProposta": cdProposta,
        "tabClienteObj": tabClienteObj.toMap(),
        "dtProposta": dtProposta,
        "txProposta": txProposta,
        "tabStatusObj": tabStatusObj.toMap(),
        "tabTipoCarregamentoObj": tabTipoCarregamentoObj.toMap(),
        "tabDestinoObj": tabDestinoObj.toMap(),
        "txLote": txLote,
        "txImportador": txImportador,
        "txCnpjImportador": txCnpjImportador,
        "txBl": txBl,
        "txContainer": txContainer,
        "listPropostasDoctos": List<dynamic>.from(listPropostasDoctos.map((x) => x.toMap())),
      };
}

class ListPropostasDocto {
  ListPropostasDocto({
    @required this.cdPropostaDocumento,
    @required this.cdProposta,
    @required this.txNomeArquivo,
    @required this.txUrl,
    @required this.cdClassificacao,
    @required this.dtCriacao,
    @required this.cdUsuarioCriacao,
  });

  final int cdPropostaDocumento;
  final int cdProposta;
  final String txNomeArquivo;
  final String txUrl;
  final int cdClassificacao;
  final String dtCriacao;
  final int cdUsuarioCriacao;

  factory ListPropostasDocto.fromJson(String str) => ListPropostasDocto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ListPropostasDocto.fromMap(Map<String, dynamic> json) => ListPropostasDocto(
        cdPropostaDocumento: json["cdPropostaDocumento"],
        cdProposta: json["cdProposta"],
        txNomeArquivo: json["txNomeArquivo"],
        txUrl: json["txUrl"],
        cdClassificacao: json["cdClassificacao"],
        dtCriacao: json["dtCriacao"],
        cdUsuarioCriacao: json["cdUsuarioCriacao"],
      );

  Map<String, dynamic> toMap() => {
        "cdPropostaDocumento": cdPropostaDocumento,
        "cdProposta": cdProposta,
        "txNomeArquivo": txNomeArquivo,
        "txUrl": txUrl,
        "cdClassificacao": cdClassificacao,
        "dtCriacao": dtCriacao,
        "cdUsuarioCriacao": cdUsuarioCriacao,
      };
}

class TabDestinoObj {
  TabDestinoObj({
    @required this.cdDestino,
    @required this.cdEmpresa,
    @required this.txDestino,
    @required this.vlDtaImportacao,
    @required this.vlExportacao,
    @required this.ckRotaAmalog,
    @required this.ckAtivo,
    @required this.cdArmazem,
    @required this.cdCodigoIbge,
  });

  final int cdDestino;
  final int cdEmpresa;
  final String txDestino;
  final double vlDtaImportacao;
  final double vlExportacao;
  final int ckRotaAmalog;
  final int ckAtivo;
  final dynamic cdArmazem;
  final int cdCodigoIbge;

  factory TabDestinoObj.fromJson(String str) => TabDestinoObj.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TabDestinoObj.fromMap(Map<String, dynamic> json) => TabDestinoObj(
        cdDestino: json["cdDestino"],
        cdEmpresa: json["cdEmpresa"],
        txDestino: json["txDestino"],
        vlDtaImportacao: json["vlDtaImportacao"].toDouble(),
        vlExportacao: json["vlExportacao"].toDouble(),
        ckRotaAmalog: json["ckRotaAmalog"],
        ckAtivo: json["ckAtivo"],
        cdArmazem: json["cdArmazem"],
        cdCodigoIbge: json["cdCodigoIbge"],
      );

  Map<String, dynamic> toMap() => {
        "cdDestino": cdDestino,
        "cdEmpresa": cdEmpresa,
        "txDestino": txDestino,
        "vlDtaImportacao": vlDtaImportacao,
        "vlExportacao": vlExportacao,
        "ckRotaAmalog": ckRotaAmalog,
        "ckAtivo": ckAtivo,
        "cdArmazem": cdArmazem,
        "cdCodigoIbge": cdCodigoIbge,
      };
}

class TabStatusObj {
  TabStatusObj({
    @required this.cdStatus,
    @required this.txStatus,
    @required this.txTipoStatus,
  });

  final int cdStatus;
  final String txStatus;
  final String txTipoStatus;

  factory TabStatusObj.fromJson(String str) => TabStatusObj.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TabStatusObj.fromMap(Map<String, dynamic> json) => TabStatusObj(
        cdStatus: json["cdStatus"],
        txStatus: json["txStatus"],
        txTipoStatus: json["txTipoStatus"],
      );

  Map<String, dynamic> toMap() => {
        "cdStatus": cdStatus,
        "txStatus": txStatus,
        "txTipoStatus": txTipoStatus,
      };
}

class TabTipoCarregamentoObj {
  TabTipoCarregamentoObj({
    @required this.cdTipoCarregamento,
    @required this.txTipoCarregamento,
  });

  final int cdTipoCarregamento;
  final String txTipoCarregamento;

  factory TabTipoCarregamentoObj.fromJson(String str) => TabTipoCarregamentoObj.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TabTipoCarregamentoObj.fromMap(Map<String, dynamic> json) => TabTipoCarregamentoObj(
        cdTipoCarregamento: json["cdTipoCarregamento"],
        txTipoCarregamento: json["txTipoCarregamento"],
      );

  Map<String, dynamic> toMap() => {
        "cdTipoCarregamento": cdTipoCarregamento,
        "txTipoCarregamento": txTipoCarregamento,
      };
}
