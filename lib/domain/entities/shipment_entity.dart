import 'dart:convert';

import 'package:meta/meta.dart';

import 'shared/shared.dart';

class ShipmentEntity {
  ShipmentEntity({
    @required this.cdCarregamento,
    @required this.dtCriacao,
    @required this.tabUsuarioCriacaoObj,
    @required this.dtPrevisaoCarregamento,
    @required this.dtCarregamento,
    @required this.tabVeiculoObj,
    @required this.tabMotoristaObj,
    @required this.tabStatusCarregamentoObj,
    @required this.vlTotalM3,
    @required this.vlTotalPeso,
    @required this.vlTotalCifReal,
    @required this.txLote,
    @required this.listCarregamentoLotes,
  });

  final int cdCarregamento;
  final String dtCriacao;
  final TabUsuarioOObj tabUsuarioCriacaoObj;
  final String dtPrevisaoCarregamento;
  final dynamic dtCarregamento;
  final TabVeiculoObj tabVeiculoObj;
  final TabMotoristaObj tabMotoristaObj;
  final TabStatusCarregamentoObj tabStatusCarregamentoObj;
  final double vlTotalM3;
  final double vlTotalPeso;
  final double vlTotalCifReal;
  final dynamic txLote;
  final List<ListCarregamentoLote> listCarregamentoLotes;

  factory ShipmentEntity.fromJson(String str) => ShipmentEntity.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ShipmentEntity.fromMap(Map<String, dynamic> json) => ShipmentEntity(
        cdCarregamento: json["cdCarregamento"],
        dtCriacao: json["dtCriacao"],
        tabUsuarioCriacaoObj: TabUsuarioOObj.fromMap(json["tabUsuarioCriacaoObj"]),
        dtPrevisaoCarregamento: json["dtPrevisaoCarregamento"],
        dtCarregamento: json["dtCarregamento"],
        tabVeiculoObj: TabVeiculoObj.fromMap(json["tabVeiculoObj"]),
        tabMotoristaObj: TabMotoristaObj.fromMap(json["tabMotoristaObj"]),
        tabStatusCarregamentoObj: TabStatusCarregamentoObj.fromMap(json["tabStatusCarregamentoObj"]),
        vlTotalM3: json["vlTotalM3"].toDouble(),
        vlTotalPeso: json["vlTotalPeso"],
        vlTotalCifReal: json["vlTotalCifReal"].toDouble(),
        txLote: json["txLote"],
        listCarregamentoLotes: List<ListCarregamentoLote>.from(json["listCarregamentoLotes"].map((x) => ListCarregamentoLote.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "cdCarregamento": cdCarregamento,
        "dtCriacao": dtCriacao,
        "tabUsuarioCriacaoObj": tabUsuarioCriacaoObj.toMap(),
        "dtPrevisaoCarregamento": dtPrevisaoCarregamento,
        "dtCarregamento": dtCarregamento,
        "tabVeiculoObj": tabVeiculoObj.toMap(),
        "tabMotoristaObj": tabMotoristaObj.toMap(),
        "tabStatusCarregamentoObj": tabStatusCarregamentoObj.toMap(),
        "vlTotalM3": vlTotalM3,
        "vlTotalPeso": vlTotalPeso,
        "vlTotalCifReal": vlTotalCifReal,
        "txLote": txLote,
        "listCarregamentoLotes": List<dynamic>.from(listCarregamentoLotes.map((x) => x.toMap())),
      };
}

class ListCarregamentoLote {
  ListCarregamentoLote({
    @required this.cdProposta,
    @required this.cdCarregamento,
    @required this.txProposta,
    @required this.tabUsuarioObj,
    @required this.tabClienteObj,
    @required this.txLote,
    @required this.vlAltura,
    @required this.vlLargura,
    @required this.vlPesoExtratoDesova,
    @required this.vlPesoBruto,
    @required this.vlM3Pack,
    @required this.vlComprimento,
    @required this.vlQtdeVolume,
    @required this.txVolume,
    @required this.vlCifReal,
  });

  final int cdProposta;
  final int cdCarregamento;
  final String txProposta;
  final TabUsuarioOObj tabUsuarioObj;
  final TabClienteObj tabClienteObj;
  final String txLote;
  final double vlAltura;
  final double vlLargura;
  final double vlPesoExtratoDesova;
  final double vlPesoBruto;
  final double vlM3Pack;
  final double vlComprimento;
  final int vlQtdeVolume;
  final String txVolume;
  final double vlCifReal;

  factory ListCarregamentoLote.fromJson(String str) => ListCarregamentoLote.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ListCarregamentoLote.fromMap(Map<String, dynamic> json) => ListCarregamentoLote(
        cdProposta: json["cdProposta"],
        cdCarregamento: json["cdCarregamento"],
        txProposta: json["txProposta"],
        tabUsuarioObj: TabUsuarioOObj.fromMap(json["tabUsuarioObj"]),
        tabClienteObj: TabClienteObj.fromMap(json["tabClienteObj"]),
        txLote: json["txLote"],
        vlAltura: json["vlAltura"],
        vlLargura: json["vlLargura"],
        vlPesoExtratoDesova: json["vlPesoExtratoDesova"],
        vlPesoBruto: json["vlPesoBruto"],
        vlM3Pack: json["vlM3Pack"].toDouble(),
        vlComprimento: json["vlComprimento"].toDouble(),
        vlQtdeVolume: json["vlQtdeVolume"],
        txVolume: json["txVolume"],
        vlCifReal: json["vlCifReal"].toDouble(),
      );

  Map<String, dynamic> toMap() => {
        "cdProposta": cdProposta,
        "cdCarregamento": cdCarregamento,
        "txProposta": txProposta,
        "tabUsuarioObj": tabUsuarioObj.toMap(),
        "tabClienteObj": tabClienteObj.toMap(),
        "txLote": txLote,
        "vlAltura": vlAltura,
        "vlLargura": vlLargura,
        "vlPesoExtratoDesova": vlPesoExtratoDesova,
        "vlPesoBruto": vlPesoBruto,
        "vlM3Pack": vlM3Pack,
        "vlComprimento": vlComprimento,
        "vlQtdeVolume": vlQtdeVolume,
        "txVolume": txVolume,
        "vlCifReal": vlCifReal,
      };
}

class TabUsuarioOObj {
  TabUsuarioOObj({
    @required this.cdUsuario,
    @required this.txApelido,
  });

  final int cdUsuario;
  final String txApelido;

  factory TabUsuarioOObj.fromJson(String str) => TabUsuarioOObj.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TabUsuarioOObj.fromMap(Map<String, dynamic> json) => TabUsuarioOObj(
        cdUsuario: json["cdUsuario"],
        txApelido: json["txApelido"],
      );

  Map<String, dynamic> toMap() => {
        "cdUsuario": cdUsuario,
        "txApelido": txApelido,
      };
}

class TabMotoristaObj {
  TabMotoristaObj({
    @required this.cdMotorista,
    @required this.txMotorista,
    @required this.txCpf,
    @required this.txCnh,
  });

  final int cdMotorista;
  final String txMotorista;
  final String txCpf;
  final String txCnh;

  factory TabMotoristaObj.fromJson(String str) => TabMotoristaObj.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TabMotoristaObj.fromMap(Map<String, dynamic> json) => TabMotoristaObj(
        cdMotorista: json["cdMotorista"],
        txMotorista: json["txMotorista"],
        txCpf: json["txCpf"],
        txCnh: json["txCnh"],
      );

  Map<String, dynamic> toMap() => {
        "cdMotorista": cdMotorista,
        "txMotorista": txMotorista,
        "txCpf": txCpf,
        "txCnh": txCnh,
      };
}

class TabStatusCarregamentoObj {
  TabStatusCarregamentoObj({
    @required this.cdStatus,
    @required this.txStatus,
  });

  final int cdStatus;
  final String txStatus;

  factory TabStatusCarregamentoObj.fromJson(String str) => TabStatusCarregamentoObj.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TabStatusCarregamentoObj.fromMap(Map<String, dynamic> json) => TabStatusCarregamentoObj(
        cdStatus: json["cdStatus"],
        txStatus: json["txStatus"],
      );

  Map<String, dynamic> toMap() => {
        "cdStatus": cdStatus,
        "txStatus": txStatus,
      };
}

class TabVeiculoObj {
  TabVeiculoObj({
    @required this.cdVeiculo,
    @required this.txVeiculo,
    @required this.txPlaca,
  });

  final int cdVeiculo;
  final String txVeiculo;
  final String txPlaca;

  factory TabVeiculoObj.fromJson(String str) => TabVeiculoObj.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TabVeiculoObj.fromMap(Map<String, dynamic> json) => TabVeiculoObj(
        cdVeiculo: json["cdVeiculo"],
        txVeiculo: json["txVeiculo"],
        txPlaca: json["txPlaca"],
      );

  Map<String, dynamic> toMap() => {
        "cdVeiculo": cdVeiculo,
        "txVeiculo": txVeiculo,
        "txPlaca": txPlaca,
      };
}
