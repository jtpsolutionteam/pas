import 'dart:convert';

import 'package:meta/meta.dart';

class TabClienteObj {
  TabClienteObj({
    @required this.cdCliente,
    @required this.txCliente,
    @required this.txCnpj,
  });

  final int cdCliente;
  final String txCliente;
  final String txCnpj;

  factory TabClienteObj.fromJson(String str) => TabClienteObj.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TabClienteObj.fromMap(Map<String, dynamic> json) => TabClienteObj(
        cdCliente: json["cdCliente"],
        txCliente: json["txCliente"],
        txCnpj: json["txCnpj"],
      );

  Map<String, dynamic> toMap() => {
        "cdCliente": cdCliente,
        "txCliente": txCliente,
        "txCnpj": txCnpj,
      };
}