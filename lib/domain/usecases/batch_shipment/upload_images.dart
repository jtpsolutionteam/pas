import 'package:meta/meta.dart';

abstract class UploadBatchSpawningImages {
  Future<void> upload({
    @required List<String> imagesInBase64,
    @required int cdProposta,
  });
}
