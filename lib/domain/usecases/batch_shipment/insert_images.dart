import 'package:meta/meta.dart';

abstract class InsertBatchShipmentImages {
  Future<void> insert({
    @required List<String> images,
    @required int cdProposta,
    @required String txProposta,
  });
}
