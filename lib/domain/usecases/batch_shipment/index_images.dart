import 'package:pas/domain/models/models.dart';

abstract class IndexBatchShipmentImages {
  Future<List<BatchShipmentWithImagesModel>> index();
}
