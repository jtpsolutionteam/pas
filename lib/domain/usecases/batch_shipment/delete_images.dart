abstract class DeleteBatchShipmentImages {
  Future<void> delete(int id);
}
