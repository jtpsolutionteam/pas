abstract class RemoveCurrentAccount {
  Future<void> remove();
}
