import 'package:pas/domain/entities/entities.dart';

abstract class IndexShipment {
  Future<List<ShipmentEntity>> index();
}
