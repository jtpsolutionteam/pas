import 'package:meta/meta.dart';
import 'package:pas/domain/entities/entities.dart';

import 'search_batch_params.dart';

abstract class SearchBatch {
  Future<List<BatchEntity>> search({
    @required String code,
    @required SearchBatchParams params,
  });
}
