import 'package:meta/meta.dart';

abstract class UploadImages {
  Future<void> upload({
    @required int cdProposta,
    @required List<String> imagesInBase64,
  });
}
