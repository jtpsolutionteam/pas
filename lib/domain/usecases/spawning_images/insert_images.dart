import 'package:meta/meta.dart';

abstract class InsertSpawningImages {
  Future<void> insert({
    @required int cdProposta,
    @required String bl,
    @required List<String> images,
  });
}
