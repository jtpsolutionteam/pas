import 'package:pas/domain/models/models.dart';

abstract class IndexSpawningImages {
  Future<List<SpawningWithImagesModel>> index();
}
