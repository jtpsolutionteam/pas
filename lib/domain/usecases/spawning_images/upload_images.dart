import 'package:meta/meta.dart';

abstract class UploadSpawningImages {
  Future<void> upload({
    @required List<String> imagesInBase64,
    @required int cdProposta,
  });
}
