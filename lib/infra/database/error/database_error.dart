enum DatabaseError {
  unexpected,
  insert,
  delete,
  read,
  update,
  access,
  truncate,
}
