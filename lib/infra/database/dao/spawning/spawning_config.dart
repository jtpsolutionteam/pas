class SpawningConfig {
  static const String spawningTableName = 'spawning';
  static const String spawningImagesTableName = 'spawning_images';

  static const String createSpawningTable = 'CREATE TABLE $spawningTableName('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'cdProposta INTEGER NOT NULL,'
      'dateCreate TEXT NOT NULL,'
      'bl TEXT NOT NULL)';

  static const String createSpawningImagesTable = 'CREATE TABLE $spawningImagesTableName('
      'idImage INTEGER PRIMARY KEY AUTOINCREMENT,'
      'idSpawning INTEGER NOT NULL,'
      'imageInBase64 TEXT NOT NULL,'
      'FOREIGN KEY (idSpawning) REFERENCES $spawningTableName(id))';
}