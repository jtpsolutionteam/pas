import 'package:meta/meta.dart';
import 'package:pas/data/database/database.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/utils/utils.dart';
import 'package:sqflite/sqflite.dart';

import '../../database.dart';

class SpawningImagesDao implements DbDeleteSpawningImages, DbInsertSpawningImages, DbIndexSpawningImages {
  SQLiteConfig sqlite = SQLiteConfig();

  final spawningTable = SpawningConfig.spawningTableName;
  final spawningImagesTable = SpawningConfig.spawningImagesTableName;

  Future<void> insert({@required int cdProposta, @required List<String> images, @required String bl}) async {
    try {
      final Database db = await (sqlite.getDatabase());

      await db.transaction((transaction) async {
        var dateCreated = DateTime.now().toString();

        int idSpawning = await transaction.insert(
          spawningTable,
          {"cdProposta": cdProposta, "dateCreate": dateCreated, "bl": bl},
        );

        for (int i = 0; i < images.length; i++) {
          await transaction.insert(
            spawningImagesTable,
            SpawningImageModel(
              idSpawning: idSpawning,
              imageInBase64: images[i],
            ).toMap(),
          );
        }
      });
      consoleLog.i('Inserção feita com sucesso!');
    } catch (error) {
      throw DatabaseError.insert;
    }
  }

  Future<void> delete(int id) async {
    try {
      final Database db = await (sqlite.getDatabase());

      await db.transaction((transaction) async {
        if (id == null) return;

        await transaction.delete(
          spawningTable,
          where: "id = ?",
          whereArgs: [id],
        );

        await transaction.delete(
          spawningImagesTable,
          where: "idSpawning = ?",
          whereArgs: [id],
        );

        consoleLog.i('Exclusão feita com sucesso!');
      });
    } catch (error) {
      throw DatabaseError.delete;
    }
  }

  Future<List<SpawningWithImagesModel>> index() async {
    try {
      final Database db = await (sqlite.getDatabase());
      var spawningItens = await db.query(spawningTable);

      List<SpawningWithImagesModel> results = [];

      for (int i = 0; i < spawningItens.length; i++) {
        var images = await db.query(
          spawningImagesTable,
          columns: ['imageInBase64'],
          where: "idSpawning = ?",
          whereArgs: [spawningItens[i]['id']],
        );

        List<String> imagesBase64 = [];
        images.forEach((img) => imagesBase64.add(img['imageInBase64']));

        SpawningWithImagesModel element = SpawningWithImagesModel(
          cdProposta: spawningItens[i]['cdProposta'],
          dateCreate: spawningItens[i]['dateCreate'],
          bl: spawningItens[i]['bl'],
          id: spawningItens[i]['id'],
          imagesInBase64: imagesBase64,
        );

        results.add(element);
      }
      return results;
    } catch (error) {
      throw DatabaseError.read;
    }
  }
}
