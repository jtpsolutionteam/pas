import 'package:meta/meta.dart';
import 'package:pas/data/database/database.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/infra/database/database.dart';
import 'package:pas/utils/utils.dart';
import 'package:sqflite/sqflite.dart';

class BatchShipmentDao implements DbIndexBatchShipmentImages, DbDeleteBatchShipmentImages, DbInsertBatchShipmentImages {
  SQLiteConfig sqlite = SQLiteConfig();

  final batchShipmentTableName = BatchShipmentConfig.batchShipmentTableName;

  final batchShipmentImagesTableName = BatchShipmentConfig.batchShipmentImagesTableName;

  Future<void> insert({
    @required List<String> images,
    @required int cdProposta,
    @required String txProposta,
  }) async {
    try {
      final Database db = await (sqlite.getDatabase());

      await db.transaction((transaction) async {
        var dateCreated = DateTime.now().toString();

        int idBatchShipment = await transaction.insert(
          batchShipmentTableName,
          {
            "dateCreate": dateCreated,
            "cdProposta": cdProposta,
            "txProposta": txProposta,
          },
        );

        for (int i = 0; i < images.length; i++) {
          await transaction.insert(
            batchShipmentImagesTableName,
            BatchShipmentImageModel(
              idBatchShipment: idBatchShipment,
              imageInBase64: images[i],
            ).toMap(),
          );
        }
      });
      consoleLog.i('Inserção feita com sucesso!');
    } catch (error) {
      throw DatabaseError.insert;
    }
  }

  Future<void> delete(int id) async {
    try {
      final Database db = await (sqlite.getDatabase());

      await db.transaction((transaction) async {
        if (id == null) return;

        await transaction.delete(
          batchShipmentTableName,
          where: "id = ?",
          whereArgs: [id],
        );

        await transaction.delete(
          batchShipmentImagesTableName,
          where: "idBatchShipment = ?",
          whereArgs: [id],
        );

        consoleLog.i('Exclusão feita com sucesso!');
      });
    } catch (error) {
      throw DatabaseError.delete;
    }
  }

  Future<List<BatchShipmentWithImagesModel>> index() async {
    try {
      final Database db = await (sqlite.getDatabase());

      var spawningItens = await db.query(batchShipmentTableName);

      List<BatchShipmentWithImagesModel> results = [];

      for (int i = 0; i < spawningItens.length; i++) {
        var images = await db.query(
          batchShipmentImagesTableName,
          columns: ['imageInBase64'],
          where: "idBatchShipment = ?",
          whereArgs: [spawningItens[i]['id']],
        );

        List<String> imagesBase64 = [];
        images.forEach((img) => imagesBase64.add(img['imageInBase64']));

        BatchShipmentWithImagesModel element = BatchShipmentWithImagesModel(
          id: spawningItens[i]['id'],
          dateCreate: spawningItens[i]['dateCreate'],
          cdProposta: spawningItens[i]['cdProposta'],
          txProposta: spawningItens[i]['txProposta'],
          imagesInBase64: imagesBase64,
        );

        results.add(element);
      }
      return results;
    } catch (error) {
      throw DatabaseError.read;
    }
  }
}
