class BatchShipmentConfig {
  static const String batchShipmentTableName = 'batch_shipment';
  static const String batchShipmentImagesTableName = 'batch_shipment_images';

  static const String createBatchShipmentTable = 'CREATE TABLE $batchShipmentTableName('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'dateCreate TEXT NOT NULL,'
      'txProposta TEXT NOT NULL,'
      'cdProposta INTEGER NOT NULL)';

  static const String createBatchShipmentImagesTable = 'CREATE TABLE $batchShipmentImagesTableName('
      'idImage INTEGER PRIMARY KEY AUTOINCREMENT,'
      'idBatchShipment INTEGER NOT NULL,'
      'imageInBase64 TEXT NOT NULL,'
      'FOREIGN KEY (idBatchShipment) REFERENCES $batchShipmentTableName(id))';
}