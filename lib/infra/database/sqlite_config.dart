import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'dao/dao.dart';
import 'database.dart';

class SQLiteConfig {
  static const databaseName = 'amalog.db';

  Future<Database> getDatabase() async {
    try {
      final String path = join(await (getDatabasesPath()), databaseName);

      return openDatabase(
        path,
        onCreate: (db, version) {
          db.execute(SpawningConfig.createSpawningTable);
          db.execute(SpawningConfig.createSpawningImagesTable);
          db.execute(BatchShipmentConfig.createBatchShipmentTable);
          db.execute(BatchShipmentConfig.createBatchShipmentImagesTable);
        },
        version: 1,
      );
    } catch (error) {
      throw DatabaseError.access;
    }
  }

  Future<int> truncateTable(String nameTable) async {
    try {
      final Database db = await getDatabase();
      return db.delete(nameTable);
    } catch (error) {
      throw DatabaseError.truncate;
    }
  }

  Future<void> dropDatabase() async {
    try {
      final String path = join(await (getDatabasesPath()), databaseName);
      await deleteDatabase(path);
    } catch (error) {
      throw DatabaseError.truncate;
    }
  }
}
