import 'dart:convert';

import 'package:http/http.dart';
import 'package:meta/meta.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/utils/utils.dart';

class HttpAdapter implements HttpClient {
  final Client client;

  HttpAdapter(this.client);

  Future<dynamic> request({
    @required String url,
    @required String method,
    Map body,
    Map headers,
  }) async {
    final defaultHeaders = headers?.cast<String, String>() ?? {}
      ..addAll({
        'content-type': 'application/json; charset=utf-8',
        'accept': 'application/json',
      });

    final jsonBody = body != null ? jsonEncode(body) : null;

    var response = Response('', 500);

    Future<Response> futureResponse;
    try {
      if (method == 'post') {
        futureResponse = client.post(url, headers: defaultHeaders, body: jsonBody);
      } else if (method == 'get') {
        futureResponse = client.get(url, headers: defaultHeaders);
      } else if (method == 'put') {
        futureResponse = client.put(url, headers: defaultHeaders, body: jsonBody);
      }
      if (futureResponse != null) {
        response = await futureResponse.timeout(Duration(seconds: 10));
      }
    } catch (error) {
      throw HttpError.serverError;
    }

    consoleLog.i(
      'Requisição HTTP: ' + response.statusCode.toString() + ' | ' + '$url | ' + response.body,
    );

    return _handleResponse(response);
  }

  dynamic _handleResponse(Response response) {
    switch (response.statusCode) {
      case 200:
        return response.body.isEmpty
            ? null
            : jsonDecode(utf8.decode(
                response.bodyBytes,
              ));
      case 204:
        return null;
      case 400:
        throw HttpError.badRequest;
      case 401:
        throw HttpError.unauthorized;
      case 403:
        throw HttpError.forbbiden;
      case 404:
        throw HttpError.notFound;
      default:
        throw HttpError.serverError;
    }
  }
}
