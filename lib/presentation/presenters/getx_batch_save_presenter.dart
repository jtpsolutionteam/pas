import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/ui/pages/pages.dart';

class GetxBatchSavePresenter extends GetxController implements BatchSavePresenter {
  final InsertSpawningImages saveSpawningImages;
  final BatchEntity batch;

  GetxBatchSavePresenter({
    @required this.saveSpawningImages,
    @required this.batch,
  });

  @override
  Future<bool> saveImages({@required List<String> imagesInBase64}) async {
    try {
      await saveSpawningImages.insert(
        cdProposta: batch.cdProposta,
        bl: batch.txBl,
        images: imagesInBase64,
      );
      return true;
    } catch (error) {
      return false;
    }
  }

  // ignore: must_call_super
  void dispose() {}
}
