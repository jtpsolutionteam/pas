import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/ui/pages/pages.dart';

class GetxIndexShipmentPresenter extends GetxController implements IndexShipmentPresenter {
  final IndexShipment indexShipment;
  final RemoveCurrentAccount removeCurrentAccount;

  GetxIndexShipmentPresenter({
    @required this.indexShipment,
    @required this.removeCurrentAccount,
  });

  var _navigateTo = RxString();
  Stream<String> get navigateToStream => _navigateTo.stream;

  @override
  Future<List<ShipmentEntity>> index({int durationInSeconds = 3}) async {
    try {
      var result = await indexShipment.index();
      return result;
    } on DomainError catch (error) {
      if (error == DomainError.invalidCredentials) {
        await removeCurrentAccount.remove();

        await Future.delayed(Duration(seconds: durationInSeconds), () {
          _navigateTo.value = '/login';
        });
        return null;
      }
      throw DomainError.unexpected;
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
