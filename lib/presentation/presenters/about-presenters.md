# Sobre os presenters

Este arquivo explica o porquê da implementação dos presenters de search e batch serem diferentes do presenter de login.

Eu não consegui utilizar o Builder na página de Container e Search para ouvir corretamente as Streams. Como assim?

Na tela de batch, por exemplo: Eu tinha configurado uma stream de erro e a UI - que estava verificando o estado dessa stream - deveria notificar o usuário com uma snackbar. O problema é: esse snackbar estava sendo chamada **diversas** vezes ao invés de uma só. 

Eu realmente não entendi o motivo desse problema ocorrer e por questões de querer entregar mais rápido o projeto, utilizei outra estratégia (FutureBuilder) para notificar o usuário de houve algum erro ou se tudo correu bem. 

A mesma coisa ocorreu na tela de Search. Os snackbars estavam aparecendo diversas vezes - um seguido do outro ao mesmo tempo - e por isso utilizei FutureBuilder novamente. 