import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/ui/pages/pages.dart';

class GetxUploadImageSpawningPresenter extends GetxController implements UploadSpawningImagesPresenter {
  final IndexSpawningImages getSpawningImages;
  final UploadImages uploadSpawningImages;
  final DeleteSpawningImages deleteSpawningImages;
  final RemoveCurrentAccount removeCurrentAccount;

  GetxUploadImageSpawningPresenter({
    @required this.getSpawningImages,
    @required this.uploadSpawningImages,
    @required this.deleteSpawningImages,
    @required this.removeCurrentAccount,
  });

  // ignore: must_call_super
  void dispose() {}

  var _navigateTo = RxString();
  Stream<String> get navigateToStream => _navigateTo.stream;

  var _uploadSuccess = false.obs;
  Stream<bool> get isUploadSuccess => _uploadSuccess.stream;

  var _uploadAllSuccess = false.obs;
  Stream<bool> get isUploadAllSuccess => _uploadAllSuccess.stream;

  var _deleteSuccess = false.obs;
  Stream<bool> get isDeleteSuccess => _deleteSuccess.stream;

  var _isLoading = false.obs;
  Stream<bool> get isLoading => _isLoading.stream;

  Future<List<SpawningWithImagesModel>> index() async {
    try {
      return await getSpawningImages.index();
    } catch (error) {
      throw DomainError.unexpected;
    }
  }

  Future<bool> uploadAllImages(List<SpawningWithImagesModel> list) async {
    try {
      _isLoading.value = true;

      for (int i = 0; i < list.length; i++) {
        await uploadImages(
          imagesInBase64: list[i].imagesInBase64,
          cdProposta: list[i].cdProposta,
          idElement: list[i].id,
        );
      }
      _uploadAllSuccess.value = true;
      return true;
    } catch (error) {
      _uploadAllSuccess.value = false;
      throw DomainError.unexpected;
    } finally {
      _isLoading.value = false;
    }
  }

  Future<bool> delete(int id) async {
    try {
      await deleteSpawningImages.delete(id);
      _deleteSuccess.value = true;

      return true;
    } catch (error) {
      _deleteSuccess.value = false;
      throw DomainError.unexpected;
    }
  }

  Future<bool> uploadImages({
    @required List<String> imagesInBase64,
    @required int cdProposta,
    @required int idElement,
    int duration = 0,
  }) async {
    try {
      await uploadSpawningImages.upload(
        imagesInBase64: imagesInBase64,
        cdProposta: cdProposta,
      );

      await deleteSpawningImages.delete(idElement);

      _uploadSuccess.value = true;
      return true;
    } on DomainError catch (error) {
      _uploadSuccess.value = false;

      if (error == DomainError.invalidCredentials) {
        await removeCurrentAccount.remove();

        await Future.delayed(Duration(seconds: duration), () {
          _navigateTo.value = '/login';
        });
        return null;
      }
      throw DomainError.unexpected;
    } catch (error) {
      _uploadSuccess.value = false;
      throw DomainError.unexpected;
    }
  }
}
