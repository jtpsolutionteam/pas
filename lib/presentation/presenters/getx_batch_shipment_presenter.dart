import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/ui/pages/pages.dart';

class GetxBatchShipmentPresenter extends GetxController implements BatchShipmentPresenter {
  final InsertBatchShipmentImages save;
  final ListCarregamentoLote batchShipment;

  GetxBatchShipmentPresenter({
    @required this.save,
    @required this.batchShipment,
  });

  @override
  Future<bool> saveImages({@required List<String> imagesInBase64}) async {
    try {
      await save.insert(
        cdProposta: batchShipment.cdProposta,
        images: imagesInBase64,
        txProposta: batchShipment.txProposta,
      );
      return true;
    } catch (error) {
      return false;
    }
  }

  // ignore: must_call_super
  void dispose() {}
}
