import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/ui/pages/pages.dart';

class GetxSearchPresenter extends GetxController implements SearchPresenter {
  final SearchBatch searchBatch;
  final RemoveCurrentAccount removeCurrentAccount;

  GetxSearchPresenter({
    @required this.searchBatch,
    @required this.removeCurrentAccount,
  });

  // ignore: must_call_super
  void dispose() {}

  var _navigateTo = RxString();
  Stream<String> get navigateToStream => _navigateTo.stream;

  Future<List<BatchEntity>> search({
    @required String parameter,
    @required SearchBatchParams typeParameter,
    int durationInSeconds = 3,
  }) async {
    try {
      var batchs = await searchBatch.search(
        code: parameter,
        params: typeParameter,
      );
      return batchs;
    } on DomainError catch (error) {
      if (error == DomainError.invalidCredentials) {
        await removeCurrentAccount.remove();

        await Future.delayed(Duration(seconds: durationInSeconds), () {
          _navigateTo.value = '/login';
        });
      }
      return null;
    }
  }
}
