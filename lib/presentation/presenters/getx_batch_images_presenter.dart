import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/ui/pages/pages.dart';

class GetxBatchImagesPresenter implements BatchImagesPresenter {
  final LoadCurrentAccount loadCurrentAccount;

  GetxBatchImagesPresenter(this.loadCurrentAccount);

  @override
  Future<AccountEntity> loadAccount() async {
    try {
      var account = await loadCurrentAccount.load();
      return account;
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
