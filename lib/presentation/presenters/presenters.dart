export './getx_batch_images_presenter.dart';
export './getx_batch_save_presenter.dart';
export './getx_batch_shipment_presenter.dart';
export './getx_index_shipment_presenter.dart';
export './getx_login_presenter.dart';
export './getx_search_presenter.dart';
export './getx_splash_presenter.dart';
export './getx_upload_image_shipment_presenter.dart';
export './getx_upload_image_spawning_presenter.dart';
