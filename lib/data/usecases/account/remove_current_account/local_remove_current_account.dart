import 'package:meta/meta.dart';
import 'package:pas/data/cache/cache.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class LocalRemoveCurrentAccount implements RemoveCurrentAccount {
  final RemoveSecureCacheStorage removeSecureCacheStorage;

  LocalRemoveCurrentAccount({@required this.removeSecureCacheStorage});

  Future<void> remove() async {
    try {
      await removeSecureCacheStorage.removeSecure();
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
