import 'package:meta/meta.dart';
import 'package:pas/data/cache/cache.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class LocalSaveCurrentAccount implements SaveCurrentAccount {
  final SaveSecureCacheStorage saveSecureCacheStorage;

  LocalSaveCurrentAccount({@required this.saveSecureCacheStorage});

  Future<void> save(AccountEntity account) async {
    try {
      await saveSecureCacheStorage.saveSecure(
          key: 'token', value: account.token);

      await saveSecureCacheStorage.saveSecure(
          key: 'id', value: account.id.toString());
          
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
