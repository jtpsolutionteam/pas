import 'package:pas/data/cache/cache.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class LocalLoadCurrentAccount implements LoadCurrentAccount {
  final FetchSecureCacheStorage fetchSecureCacheStorage;

  LocalLoadCurrentAccount(this.fetchSecureCacheStorage);

  Future<AccountEntity> load() async {
    try {
      final token = await fetchSecureCacheStorage.fetchSecure('token');
      final id = await fetchSecureCacheStorage
          .fetchSecure('id')
          .then((idString) => int.parse(idString));

      return AccountEntity(
        token: token,
        id: id,
      );
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
