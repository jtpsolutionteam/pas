import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class RemoteSearchBatch implements SearchBatch {
  final HttpClient httpClient;
  final String urlToBatch;
  final String urlToBL;
  final LoadCurrentAccount fetchCache;

  RemoteSearchBatch({
    @required this.httpClient,
    @required this.urlToBatch,
    @required this.urlToBL,
    @required this.fetchCache,
  });

  Future<List<BatchEntity>> search({
    @required String code,
    @required SearchBatchParams params,
  }) async {
    try {
      final token = await fetchCache.load().then((value) => value.token);        

      final url = _addCodeToUrl(_returnCorrectUrl(params), code);

      final List<dynamic> response = await httpClient.request(
        url: url,
        method: 'get',
        headers: {
          'Authorization': 'Bearer $token',
        },
      );

      var ship = response.map((e) => BatchEntity.fromJson(json.encode(e))).toList();

      return ship;
    } on HttpError catch (error) {
      if (error == HttpError.unauthorized) {
        throw DomainError.invalidCredentials;
      } else {
        throw DomainError.unexpected;
      }
    }
  }

  String _addCodeToUrl(String url, String code) {
    return url + code;
  }

  String _returnCorrectUrl(SearchBatchParams typeParams) {
    if (typeParams == SearchBatchParams.batch) {
      return urlToBatch;
    } else {
      return urlToBL;
    }
  }
}
