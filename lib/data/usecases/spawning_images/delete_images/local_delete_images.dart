import 'package:pas/data/database/database.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class LocalDeleteSpawningImages implements DeleteSpawningImages {
  final DbDeleteSpawningImages deleteImages;

  LocalDeleteSpawningImages(this.deleteImages);

  Future<void> delete(int id) async {
    try {
      await deleteImages.delete(id);
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
