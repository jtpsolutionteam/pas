import 'package:pas/data/database/database.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/domain/models/models.dart';

class LocalIndexSpawningImages implements IndexSpawningImages {
  final DbIndexSpawningImages listImages;

  LocalIndexSpawningImages(this.listImages);

  Future<List<SpawningWithImagesModel>> index() async {
    try {
      return await listImages.index();
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
