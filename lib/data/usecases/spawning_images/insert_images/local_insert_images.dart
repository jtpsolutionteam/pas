import 'package:meta/meta.dart';
import 'package:pas/data/database/database.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class LocalInsertSpawningImages implements InsertSpawningImages {
  final DbInsertSpawningImages insertImages;

  LocalInsertSpawningImages(this.insertImages);

  Future<void> insert({
    @required int cdProposta,
    @required String bl,
    @required List<String> images,
  }) async {
    try {
      await insertImages.insert(
        cdProposta: cdProposta,
        bl: bl,
        images: images,
      );
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
