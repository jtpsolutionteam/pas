import 'package:meta/meta.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

///Convert in API body params
class RemoteUploadImagesApiParams {
  RemoteUploadImagesApiParams({
    @required this.cdProposta,
    @required this.cdUsuario,
    @required this.listFotos,
  });

  final int cdProposta;
  final int cdUsuario;
  final List<ListFoto> listFotos;

  Map<String, dynamic> toMap() {
    return {
      "cdProposta": cdProposta,
      "listFotos": List<dynamic>.from(listFotos.map((x) => x.toMap())),
      "cdUsuario": cdUsuario,
    };
  }
}

class ListFoto {
  final String imagePath;
  ListFoto({@required this.imagePath});
  Map<String, dynamic> toMap() => {"txArquivo": imagePath};
}

class RemoteUploadImages implements UploadImages {
  final HttpClient httpClient;
  final LoadCurrentAccount fetchCache;
  final String url;

  RemoteUploadImages({
    @required this.httpClient,
    @required this.fetchCache,
    @required this.url,
  });

  Future<void> upload({
    @required List<String> imagesInBase64,
    @required int cdProposta,
  }) async {
    try {
      var account = await fetchCache.load();
      var token = account.token;
      var cdUsuario = account.id;

      List<ListFoto> imagesPaths = [];

      imagesInBase64.forEach((e) => imagesPaths.add(ListFoto(imagePath: e)));

      var body = RemoteUploadImagesApiParams(
        cdProposta: cdProposta,
        listFotos: imagesPaths,
        cdUsuario: cdUsuario,
      ).toMap();

      await httpClient.request(
        url: url,
        method: 'post',
        headers: {'Authorization': 'Bearer $token'},
        body: body,
      );
      return;
    } on HttpError catch (error) {
      if (error == HttpError.unauthorized) throw DomainError.invalidCredentials;
      throw DomainError.unexpected;
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
