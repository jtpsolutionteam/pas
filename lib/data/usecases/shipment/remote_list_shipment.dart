import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class RemoteIndexShipment implements IndexShipment {
  final HttpClient httpClient;
  final String url;
  final LoadCurrentAccount fetchCache;

  RemoteIndexShipment({
    @required this.httpClient,
    @required this.url,
    @required this.fetchCache,
  });

  Future<List<ShipmentEntity>> index() async {
    try {
      final token = await fetchCache.load().then((value) => value.token);

      final List<dynamic> response = await httpClient.request(
        url: url,
        method: 'get',
        headers: {
          'Authorization': 'Bearer $token',
        },
      );

      final listShipment = response
          .map(
            (e) => ShipmentEntity.fromJson(json.encode(e)),
          )
          .toList();

      return listShipment;
    } on HttpError catch (error) {
      if (error == HttpError.unauthorized) {
        throw DomainError.invalidCredentials;
      } else {
        throw DomainError.unexpected;
      }
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
