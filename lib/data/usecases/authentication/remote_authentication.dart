import 'package:meta/meta.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/data/models/models.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class RemoteAuthentication implements Authentication {
  final HttpClient httpClient;
  final String url;

  RemoteAuthentication({
    @required this.httpClient,
    @required this.url,
  });

  Future<AccountEntity> auth(AuthenticationParams params) async {
    try {
      final queryParams = RemoteAuthenticationParams.fromDomain(params);

      final apiUrl = _urlWithQueryparams(
        url,
        queryParams.email,
        queryParams.password,
      );

      final httpResponse = await httpClient.request(
        url: apiUrl,
        method: 'post',
        headers: {'Authorization': 'Basic YW1hbG9nOmFtYWxvZyoyMDIw'},
      );

      final account = RemoteAccountModel.fromJson(httpResponse).toEntity();

      return account;
    } on HttpError catch (error) {
      if (error == HttpError.unauthorized || error == HttpError.badRequest) {
        throw DomainError.invalidCredentials;
      } else {
        throw DomainError.unexpected;
      }
    }
  }

  String _urlWithQueryparams(String url, String email, String password) {
    return url + 'username=' + email + '&password=' + password + '&grant_type=password';
  }
}

class RemoteAuthenticationParams {
  //API-specific params
  final String email;
  final String password;

  RemoteAuthenticationParams({
    @required this.email,
    @required this.password,
  });

  factory RemoteAuthenticationParams.fromDomain(AuthenticationParams entity) {
    return RemoteAuthenticationParams(
      email: entity.email,
      password: entity.password,
    );
  }

  Map toJson() {
    return {
      "username": email,
      "password": password,
      "grant_type": "password",
    };
  }
}
