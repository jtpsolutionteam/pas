import 'package:meta/meta.dart';
import 'package:pas/data/database/database.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class LocalInsertBatchShipmentImages implements InsertBatchShipmentImages {
  final DbInsertBatchShipmentImages insertImages;

  LocalInsertBatchShipmentImages(this.insertImages);

  Future<void> insert({
    @required List<String> images,
    @required int cdProposta,
    @required String txProposta,
  }) async {
    try {
      await insertImages.insert(
        cdProposta: cdProposta,
        images: images,
        txProposta: txProposta,
      );
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
