import 'package:pas/data/database/database.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class LocalIndexBatchShipmentImages implements IndexBatchShipmentImages {
  final DbIndexBatchShipmentImages listImages;

  LocalIndexBatchShipmentImages(this.listImages);

  Future<List<BatchShipmentWithImagesModel>> index() async {
    try {
      return await listImages.index();
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
