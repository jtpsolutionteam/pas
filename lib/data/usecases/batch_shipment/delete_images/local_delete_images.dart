import 'package:pas/data/database/database.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/domain/usecases/usecases.dart';

class LocalDeleteBatchShipmentImages implements DeleteBatchShipmentImages {
  final DbDeleteBatchShipmentImages deleteImages;

  LocalDeleteBatchShipmentImages(this.deleteImages);

  Future<void> delete(int id) async {
    try {
      await deleteImages.delete(id);
    } catch (error) {
      throw DomainError.unexpected;
    }
  }
}
