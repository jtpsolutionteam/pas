import 'package:meta/meta.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/domain/entities/entities.dart';

class RemoteAccountModel {
  final String accessToken;
  final int cdUsuario;

  RemoteAccountModel({
    @required this.accessToken,
    @required this.cdUsuario,
  });

  factory RemoteAccountModel.fromJson(Map json) {
    if (!json.keys.toSet().containsAll(['access_token', 'cdUsuario'])) {
      throw HttpError.invalidData;
    }

    return RemoteAccountModel(
      accessToken: json['access_token'],
      cdUsuario: json['cdUsuario'],
    );
  }

  AccountEntity toEntity() {
    return AccountEntity(
      token: accessToken,
      id: cdUsuario,
    );
  }
}
