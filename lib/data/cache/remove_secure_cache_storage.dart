abstract class RemoveSecureCacheStorage {
  Future<void> removeSecure();
}
