import 'package:pas/domain/models/models.dart';

abstract class DbIndexBatchShipmentImages {
  Future<List<BatchShipmentWithImagesModel>> index();
}
