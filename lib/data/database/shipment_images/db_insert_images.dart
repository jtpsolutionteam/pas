import 'package:meta/meta.dart';

abstract class DbInsertBatchShipmentImages {
  Future<void> insert({
    @required List<String> images,
    @required int cdProposta,
    @required String txProposta,
  });
}
