abstract class DbDeleteBatchShipmentImages {
  Future<void> delete(int id);
}
