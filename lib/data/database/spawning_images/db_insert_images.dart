import 'package:meta/meta.dart';

abstract class DbInsertSpawningImages {
  Future<void> insert({
    @required int cdProposta,
    @required String bl,
    @required List<String> images,
  });
}
