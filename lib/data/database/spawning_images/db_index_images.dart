import 'package:pas/domain/models/models.dart';

abstract class DbIndexSpawningImages {
  Future<List<SpawningWithImagesModel>> index();
}
