import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/pages/pages.dart';

class SplashPage extends StatelessWidget {
  final SplashPresenter presenter;

  const SplashPage({Key key, @required this.presenter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    presenter.checkAccount();

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorDark,
      body: Builder(
        builder: (context) {
          handleNavigation(presenter.navigateToStream, clear: true);
          
          return Center(
            child: Image(
              width: 300,
              height: 300,
              image: AssetImage(
                'lib/ui/assets/images/amalog-color.png',
              ),
            ),
          );
        },
      ),
    );
  }
}
