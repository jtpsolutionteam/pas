import 'package:flutter/material.dart';
import 'package:pas/ui/components/components.dart';

class NoPendingUploads extends StatelessWidget {
  const NoPendingUploads({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 200,
        width: 200,
        child: Column(
          children: [
            Icon(Icons.assignment_turned_in_outlined, size: 48),
            Space(16),
            Text(
              'Não há nenhum envio pendente!',
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}