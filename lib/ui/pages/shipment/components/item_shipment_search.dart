import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/pages/pages.dart';

class ItemShipmentSearch extends StatelessWidget {
  const ItemShipmentSearch({
    Key key,
    @required this.item,
  }) : super(key: key);

  final ShipmentEntity item;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.to(() => ItemShipmentPage(shipment: item)),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))),
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
        margin: EdgeInsets.only(bottom: 12),
        child: Row(
          children: [
            Expanded(
              flex: 6,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Veículo",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.black,
                    ),
                  ),
                  Space(8),
                  Text(
                    item.tabVeiculoObj.txVeiculo + " | " + item.tabVeiculoObj.txPlaca,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                  ),
                  Space(12),
                  Text(
                    "Motorista",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.black,
                    ),
                  ),
                  Space(8),
                  Text(
                    item.tabMotoristaObj.txMotorista,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                  ),
                  Space(12),
                  Text(
                    "Data da previsão de carregamento",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.black,
                    ),
                  ),
                  Space(8),
                  Text(
                    item.dtPrevisaoCarregamento,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Icon(
                Icons.open_in_new,
                color: Colors.black,
              ),
            )
          ],
        ),
      ),
    );
  }
}
