import 'package:flutter/material.dart';
import 'package:pas/ui/components/components.dart';

class ShipmentCard extends StatelessWidget {
  final String title;
  final List<String> content;

  const ShipmentCard({
    Key key,
    @required this.title,
    @required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(4)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          Space(12),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: content.length,
            itemBuilder: (context, index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    content[index],
                    style: TextStyle(fontSize: 14, height: 1.6),
                  ),
                  Space(8)
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
