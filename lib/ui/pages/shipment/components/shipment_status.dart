import 'package:flutter/material.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/theme/theme.dart';

class ShipmentStatus extends StatelessWidget {
  const ShipmentStatus({
    Key key,
    @required this.shipment,
  }) : super(key: key);

  final ShipmentEntity shipment;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 16),
      padding: EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 32,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(4)),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Icon(Icons.info, size: 32, color: AppTheme.colors(type: ColorsType.primary)),
          ),
          Expanded(
            flex: 2,
            child: Text(
              shipment.tabStatusCarregamentoObj.txStatus,
              style: TextStyle(
                color: AppTheme.colors(type: ColorsType.primary),
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          )
        ],
      ),
    );
  }
}
