import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/pages/pages.dart';
import 'package:pas/ui/theme/theme.dart';

class ShipmentBatchItem extends StatelessWidget {
  const ShipmentBatchItem({
    Key key,
    @required this.list,
  }) : super(key: key);

  final ListCarregamentoLote list;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.to(
        () => BatchShipmentPage(
          batchShipment: list,
          presenter: makeGetxBatchShipmentPresenterFactory(list),
        ),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(4)),
        ),
        margin: EdgeInsets.only(bottom: 12),
        padding: EdgeInsets.all(12),
        child: Row(
          children: [
            Expanded(
              flex: 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    list.txProposta,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: AppTheme.colors(type: ColorsType.secundary)),
                  ),
                  Space(8),
                  Text(
                    "Lote: " + list.txLote,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: AppTheme.colors(type: ColorsType.secundary)),
                  ),
                  Space(16),
                  Flexible(
                    child: Text(
                      list.txVolume,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(child: Icon(Icons.chevron_right_sharp)),
          ],
        ),
      ),
    );
  }
}
