import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/pages/pages.dart';
import 'package:pas/utils/utils.dart';

import '../../shipment.dart';

class BatchShipmentPage extends StatefulWidget {
  final ListCarregamentoLote batchShipment;
  final BatchShipmentPresenter presenter;

  const BatchShipmentPage({
    Key key,
    @required this.batchShipment,
    @required this.presenter,
  }) : super(key: key);

  @override
  _BatchShipmentPageState createState() => _BatchShipmentPageState();
}

class _BatchShipmentPageState extends State<BatchShipmentPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<String> images;
  final _picker = ImagePicker();
  Future<bool> successUpload;

  @override
  void initState() {
    images = [];
    successUpload = null;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColorDark,
        title: Text(widget.batchShipment.txProposta),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('Carregar imagem'),
        icon: Icon(Icons.photo_library),
        onPressed: () async {
          _showGetImageModal(context);
        },
      ),      
      body: SingleChildScrollView(
        padding: EdgeInsets.only(left: 12, right: 12, top: 24, bottom: 100),
        child: Column(
          children: [
            ShipmentCard(
              title: "Lote",
              content: [widget.batchShipment.txLote],
            ),
            ShipmentCard(
              title: "Cliente",
              content: [
                "Nome: " + widget.batchShipment.tabClienteObj.txCliente,
                "CNPJ: " + widget.batchShipment.tabClienteObj.txCnpj,
              ],
            ),
            ShipmentCard(
              title: "Características",
              content: [
                "Altura: " + widget.batchShipment.vlAltura.toString(),
                "Largura: " + widget.batchShipment.vlLargura.toString(),
                "Comprimento: " + widget.batchShipment.vlComprimento.toString(),
                "Peso bruto: " + widget.batchShipment.vlPesoBruto.toString(),
                "Peso extrato desova: " + widget.batchShipment.vlPesoExtratoDesova.toString(),
                "CIF Real: " + widget.batchShipment.vlCifReal.toString(),
                "M3 Pack: " + widget.batchShipment.vlM3Pack.toString(),
              ],
            ),
            ShipmentCard(
              title: "Volume",
              content: [
                "Descrição: " + widget.batchShipment.txVolume,
                "Quantidade: " + widget.batchShipment.vlQtdeVolume.toString(),
              ],
            ),
            ColumnVisibility(
              visible: images.length > 0,
              children: [
                Text(
                  "Imagens",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Space(24),
                GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: gridTemplate(),
                  itemCount: images.length,
                  itemBuilder: (context, index) {
                    if (images[index] == null) return EmptyWidget();

                    return CardImageFile(
                      imagePath: images[index],
                      onTap: () async {
                        final mustExclude = await navigateToDetailsImagePage(context, images[index]);

                        if (mustExclude == true && mustExclude != null) {
                          _removeImage(index);
                        }
                      },
                    );
                  },
                ),
                Space(24),
                FutureBuilder<bool>(
                  future: successUpload,
                  builder: (context, snapshot) {
                    if (successUpload == null) {
                      return MaxWidth(
                        child: ElevatedButton(
                          onPressed: () => _save(),
                          child: Text('Salvar fotos'),
                        ),
                      );
                    }
                    if (snapshot.hasError) {
                      return ImageSaveMessage(
                        state: ImageSaveMessageState.error,
                      );
                    }
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        break;
                      case ConnectionState.active:
                        break;
                      case ConnectionState.done:
                        return ImageSaveMessage(
                          state: ImageSaveMessageState.success,
                        );
                        break;
                      case ConnectionState.waiting:
                        return ImageSaveMessage(
                          state: ImageSaveMessageState.loading,
                        );
                        break;
                    }
                    return EmptyWidget();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future _showGetImageModal(BuildContext context) {
    return showModalBottomSheet(
      context: context,
      isDismissible: true,
      builder: (context) {
        return Container(
          height: 120,
          child: Column(
            children: [
              ListTile(
                title: Text('Tirar uma foto'),
                leading: Icon(Icons.camera_alt),
                onTap: () async {
                  _getImage(ImageSource.camera);
                },
              ),
              ListTile(
                title: Text('Abrir a galeria'),
                leading: Icon(Icons.image),
                onTap: () async {
                  _getImage(ImageSource.gallery);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  void _getImage(ImageSource source) async {
    try {
      final PickedFile pickedFile = await _picker.getImage(
        source: source,
        imageQuality: 100,
        maxWidth: 1000,
      );

      if (pickedFile == null) return;

      if (pickedFile.path != '') {
        _addImage(pickedFile.path);
      }
    } catch (error) {
      AlertMessage.show(
        title: 'Aviso',
        message: 'O aplicativo para funcionar corretamente, precisa da sua permissão para acessar a câmera, seu microfone e sua galeria.',
        context: context,
      );
    }
  }

  void _addImage(imageStringPath) {
    setState(() {
      images.add(imageStringPath);
    });
  }

  void _removeImage(int index) {
    setState(() {
      images.remove(images[index]);
    });
  }

  void _save() async {
    var imagesInBase64 = await convertImagesToBase64(images);

    setState(() {
      successUpload = widget.presenter.saveImages(imagesInBase64: imagesInBase64);
    });
  }
}
