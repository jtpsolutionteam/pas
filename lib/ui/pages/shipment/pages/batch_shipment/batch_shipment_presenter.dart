abstract class BatchShipmentPresenter {
  Future<bool> saveImages({List<String> imagesInBase64});
  void dispose();
}
