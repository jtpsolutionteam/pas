import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/pages/pages.dart';

class IndexShipmentPage extends StatelessWidget {
  final IndexShipmentPresenter presenter;

  const IndexShipmentPage({Key key, @required this.presenter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _navigateToUploadPage() {
      final uploadPage = makeUploadShipmentImagesPageFactory();
      Get.to(() => uploadPage);
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColorDark,
        title: Text('Carregamentos'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(
              Icons.cloud_upload_rounded,
              color: Colors.white,
            ),
            tooltip: 'Upload de imagens',
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onPressed: _navigateToUploadPage,
          ),
        ],
      ),
      body: Builder(builder: (context) {
        handleNavigation(presenter.navigateToStream, clear: true);

        return FutureBuilder<List<ShipmentEntity>>(
          future: presenter.index(),
          builder: (context, snapshot) {
            if (snapshot.hasError) return EmptyWidget();

            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return EmptyWidget();
                break;
              case ConnectionState.active:
                break;
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator());
                break;
              case ConnectionState.done:
                if (snapshot.data.length == 0) return EmptyWidget();

                return ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 24),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    var item = snapshot.data[index];
                    return ItemShipmentSearch(item: item);
                  },
                );
                break;
            }
            return EmptyWidget();
          },
        );
      }),
    );
  }
}
