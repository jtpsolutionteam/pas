import 'package:pas/domain/entities/entities.dart';

abstract class IndexShipmentPresenter {
  Stream<String> get navigateToStream;

  Future<List<ShipmentEntity>> index({int durationInSeconds});
}
