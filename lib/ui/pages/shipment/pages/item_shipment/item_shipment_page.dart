import 'package:flutter/material.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/pages/pages.dart';

class ItemShipmentPage extends StatelessWidget {
  final ShipmentEntity shipment;

  const ItemShipmentPage({Key key, @required this.shipment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColorDark,
        title: Text('Carregamento'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ShipmentCard(
              title: "Veículo",
              content: [
                "Nome: " + shipment.tabVeiculoObj.txVeiculo,
                "Placa: " + shipment.tabVeiculoObj.txPlaca,
              ],
            ),
            ShipmentCard(
              title: "Motorista",
              content: [
                "Nome: " + shipment.tabMotoristaObj.txMotorista,
                "CPF: " + shipment.tabMotoristaObj.txCpf,
                "CNH: " + shipment.tabMotoristaObj.txCnh,
              ],
            ),
            ShipmentCard(
              title: "Data de previsão",
              content: [shipment.dtPrevisaoCarregamento],
            ),
            ShipmentCard(
              title: "Características",
              content: [
                "Peso total: " + shipment.vlTotalPeso.toString(),
                "Total CIF Mercadoria: " + shipment.vlTotalCifReal.toString(),
                "Total M³: " + shipment.vlTotalM3.toString(),
              ],
            ),
            Space(16),
            Text(
              "Lotes para carregamento",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Space(16),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: shipment.listCarregamentoLotes.length,
              itemBuilder: (context, index) {
                return ShipmentBatchItem(list: shipment.listCarregamentoLotes[index]);
              },
            )
          ],
        ),
      ),
    );
  }
}
