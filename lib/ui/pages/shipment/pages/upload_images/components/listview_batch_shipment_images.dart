import 'package:flutter/material.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/ui/pages/pages.dart';

import 'components.dart';

class ListViewBatchShipmentImages extends StatelessWidget {
  final List<BatchShipmentWithImagesModel> list;
  final UploadImagesShipmentPresenter presenter;

  const ListViewBatchShipmentImages({
    Key key,
    this.list,
    this.presenter,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return BatchShipmentImagesInDatabase(
            batch: list[index],
            presenter: presenter,
          );
        },
      ),
    );
  }
}
