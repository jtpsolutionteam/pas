import 'package:pas/domain/models/models.dart';
import 'package:pas/ui/pages/pages.dart';

abstract class UploadImagesShipmentPresenter extends UploadImagesPresenter {
  Future<bool> uploadAllImages(List<BatchShipmentWithImagesModel> list);
}
