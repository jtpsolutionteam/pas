import 'package:flutter/material.dart';
import 'package:pas/domain/helpers/domain_error.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/pages/pages.dart';

import 'components/components.dart';

class UploadImagesShipmentPage extends StatefulWidget {
  final UploadImagesShipmentPresenter presenter;

  const UploadImagesShipmentPage({
    Key key,
    @required this.presenter,
  }) : super(key: key);

  @override
  _UploadImagesShipmentPageState createState() => _UploadImagesShipmentPageState();
}

class _UploadImagesShipmentPageState extends State<UploadImagesShipmentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Upload de imagens'),
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      body: Builder(builder: (context) {
        widget.presenter.isDeleteSuccess.listen((event) {
          if (event == true) {
            SnackBarMessage.show(
              context: context,
              type: DomainError.none,
              message: "Imagens deletadas com sucesso!",
            );
            setState(() {});
          }
        });
        widget.presenter.isUploadAllSuccess.listen((event) {
          if (event == true) {
            SnackBarMessage.show(
              context: context,
              type: DomainError.none,
              message: "As imagens foram enviadas com sucesso!",
            );
            setState(() {});
          }
        });
        widget.presenter.isLoading.listen((event) {
          if (event == true) {
            showLoading(context);
          } else {
            hideLoading(context);
          }
        });

        return FutureBuilder<List<BatchShipmentWithImagesModel>>(
          future: widget.presenter.index(),
          builder: (context, snapshot) {
            if (snapshot.hasError) return EmptyWidget();

            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return NoPendingUploads();
                break;
              case ConnectionState.active:
                break;
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator());
                break;
              case ConnectionState.done:
                if (snapshot.data.length == 0) return NoPendingUploads();

                return Padding(
                  padding: EdgeInsets.only(left: 12, right: 12, top: 24, bottom: 60),
                  child: Column(
                    children: [
                      MaxWidth(
                        child: TextButton(
                          onPressed: () async {
                            if (snapshot.data != null && snapshot.data.length != 0) {
                              await widget.presenter.uploadAllImages(snapshot.data);
                            }
                          },
                          child: Text('Enviar fotos'),
                        ),
                      ),
                      Space(32),
                      ListViewBatchShipmentImages(
                        list: snapshot.data,
                        presenter: widget.presenter,
                      )
                    ],
                  ),
                );
                break;
            }
            return EmptyWidget();
          },
        );
      }),
    );
  }
}
