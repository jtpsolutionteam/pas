import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/protocols/protocols.dart';

class SpawningPage extends StatefulWidget {
  const SpawningPage({
    Key key,
  }) : super(key: key);

  @override
  _SpawningPageState createState() => _SpawningPageState();
}

class _SpawningPageState extends State<SpawningPage> {
  int _selectedIndex = 0;

  List<Widget> pages = [
    makeSearchPage(SearchPageNavigateTo.batchSaveImages),
    makeSearchPage(SearchPageNavigateTo.batchViewImages),
  ];

  void _navigateToUploadPage() {
    final uploadPage = makeUploadImagesSpawningPageFactory();
    Get.to(() => uploadPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColorDark,
        title: Text('Desova'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(
              Icons.cloud_upload_rounded,
              color: Colors.white,
            ),
            tooltip: 'Upload de imagens',
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onPressed: _navigateToUploadPage,
          ),
        ],
      ),
      body: pages.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Theme.of(context).primaryColorDark,
        showUnselectedLabels: true,
        iconSize: 24,
        selectedFontSize: 14,
        unselectedFontSize: 14,
        fixedColor: Colors.white,
        unselectedItemColor: Colors.grey[600],
        items: _makeBottomNavigationBar(),
        onTap: _onItemTapped,
        currentIndex: _selectedIndex,
      ),
    );
  }

  List<BottomNavigationBarItem> _makeBottomNavigationBar() {
    return [
      BottomNavigationBarItem(
        icon: Icon(Icons.image),
        label: 'Salvar imagens',
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.search),
        label: 'Consultar imagens',
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
    ];
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
