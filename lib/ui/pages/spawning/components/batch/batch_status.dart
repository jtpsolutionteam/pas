import 'package:flutter/material.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/theme/theme.dart';

class BatchStatus extends StatelessWidget {
  const BatchStatus({
    Key key,
    @required this.batch,
  }) : super(key: key);

  final BatchEntity batch;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 16),
      padding: EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 32,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(4)),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Icon(Icons.info, size: 32, color: AppTheme.colors(type: ColorsType.primary)),
          ),
          Expanded(
            flex: 2,
            child: Text(
              batch.tabStatusObj.txStatus.toString(),
              style: TextStyle(
                color: AppTheme.colors(type: ColorsType.primary),
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          )
        ],
      ),
    );
  }
}
