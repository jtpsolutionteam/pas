import 'package:flutter/material.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/ui/components/components.dart';

import 'batch_card.dart';
import 'batch_status.dart';

class BatchInformation extends StatelessWidget {
  final BatchEntity batch;

  const BatchInformation({Key key, this.batch}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BatchStatus(batch: batch),
        BatchCard(
          title: 'Código do contêiner',
          content: batch.txContainer,
        ),
        BatchCard(
          title: 'Cliente',
          content: batch.tabClienteObj.txCliente,
          small: 'CNPJ: ' + batch.tabClienteObj.txCnpj,
        ),
        BatchCard(
          title: 'Destino',
          content: batch.tabDestinoObj.txDestino,
        ),
        BatchCard(
          title: 'Importador',
          content: batch.txImportador,
          small: 'CNPJ: ' + batch.txCnpjImportador,
        ),
        BatchCard(
          title: 'Lote',
          content: batch.txLote,
        ),
        Space(32),
      ],
    );
  }
}
