import 'package:flutter/material.dart';
import 'package:pas/ui/components/components.dart';

class BatchCard extends StatelessWidget {
  final String title;
  final String content;
  final String small;

  const BatchCard({
    Key key,
    @required this.title,
    @required this.content,
    this.small = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          Space(12),
          Text(
            content,
            style: TextStyle(fontSize: 14, height: 1.6),
          ),
          Space(4),
          small != ''
              ? Text(
                  small,
                  style: TextStyle(
                    fontSize: 14,
                    color: Color(0xFF979797),
                  ),
                )
              : EmptyWidget(),
        ],
      ),
    );
  }
}
