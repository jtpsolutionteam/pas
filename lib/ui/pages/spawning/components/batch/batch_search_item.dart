import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/pages/pages.dart';
import 'package:pas/ui/protocols/protocols.dart';

class BatchSearchItem extends StatelessWidget {
  final BatchEntity batch;
  final SearchPageNavigateTo searchPageNavigateTo;

  const BatchSearchItem({
    Key key,
    @required this.batch,
    @required this.searchPageNavigateTo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (searchPageNavigateTo == SearchPageNavigateTo.batchSaveImages) {
          Get.to(
            () => BatchSavePage(
              batch: batch,
              presenter: makeBatchSavePresenterFactory(batch: batch),
            ),
          );
        } else {
          Get.to(
            () => BatchImagesPage(
              batch: batch,
              presenter: makeBatchImagesPresenterFactory(),
            ),
          );
        }
      },
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))),
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
        margin: EdgeInsets.only(bottom: 16),
        child: Row(
          children: [
            Expanded(
              flex: 6,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    batch.txBl,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.black,
                    ),
                  ),
                  Space(8),
                  Text(
                    "Nº de lote: " + batch.txLote,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black87,
                    ),
                  ),
                  Space(12),
                  Text(batch.tabClienteObj.txCliente),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Icon(
                Icons.open_in_new,
                color: Colors.black,
              ),
            )
          ],
        ),
      ),
    );
  }
}
