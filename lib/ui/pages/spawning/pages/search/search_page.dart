import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/protocols/protocols.dart';
import 'package:provider/provider.dart';

import '../pages.dart';
import 'components/components.dart';

class SearchPage extends StatefulWidget {
  final SearchPresenter presenter;
  final SearchPageNavigateTo searchPageNavigateTo;

  const SearchPage({
    Key key,
    @required this.presenter,
    @required this.searchPageNavigateTo,
  }) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  var searchTextfieldController;
  FocusNode searchTxtFNode;
  VisibleTextField visibleTextfield;
  Future<List<BatchEntity>> ship;

  @override
  void initState() {
    searchTxtFNode = FocusNode();
    searchTextfieldController = TextEditingController();
    visibleTextfield = VisibleTextField.bl;
    ship = null;
    super.initState();
  }

  @override
  void dispose() {
    searchTxtFNode.dispose();
    searchTextfieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        handleNavigation(widget.presenter.navigateToStream, clear: true);

        return Provider(
          create: (_) => widget.searchPageNavigateTo,
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 24),
            physics: ScrollPhysics(),
            child: Column(
              children: [
                TextfieldSearch(
                  controller: searchTextfieldController,
                  focus: searchTxtFNode,
                  isVisible: visibleTextfield == VisibleTextField.bl ? true : false,
                  type: TypeTextFieldSearch.bl,
                  onSearch: () {
                    hideKeyboard(context);
                    searchTxtFNode.unfocus();

                    setState(() {
                      ship = widget.presenter.search(
                        parameter: searchTextfieldController.text,
                        typeParameter: SearchBatchParams.bl,
                        durationInSeconds: 3,
                      );
                    });
                  },
                ),
                TextfieldSearch(
                  controller: searchTextfieldController,
                  focus: searchTxtFNode,
                  isVisible: visibleTextfield == VisibleTextField.batch ? true : false,
                  type: TypeTextFieldSearch.batch,
                  onSearch: () async {
                    hideKeyboard(context);
                    searchTxtFNode.unfocus();

                    setState(() {
                      ship = widget.presenter.search(
                        parameter: searchTextfieldController.text,
                        typeParameter: SearchBatchParams.batch,
                        durationInSeconds: 3,
                      );
                    });
                  },
                ),
                Space(16),
                TextButton(
                  child: Text(returnBtnMessage()),
                  onPressed: () => toggle(),
                ),
                Space(64),
                ship != null ? SearchResult(ship: ship) : EmptyWidget(),
              ],
            ),
          ),
        );
      },
    );
  }

  void toggle() {
    searchTextfieldController.value = TextEditingValue.empty;

    setState(() {
      if (visibleTextfield == VisibleTextField.bl) {
        visibleTextfield = VisibleTextField.batch;
      } else if (visibleTextfield == VisibleTextField.batch) {
        visibleTextfield = VisibleTextField.bl;
      }
    });
  }

  String returnBtnMessage() {
    if (visibleTextfield == VisibleTextField.bl) {
      return 'Pesquisar usando o lote';
    } else if (visibleTextfield == VisibleTextField.batch) {
      return 'Pesquisar usando o código BL';
    } else {
      return '-';
    }
  }
}
