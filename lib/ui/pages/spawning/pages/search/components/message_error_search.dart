import 'package:flutter/material.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/theme/theme.dart';

class MessageSearchError extends StatelessWidget {
  final DomainError error;

  const MessageSearchError({
    Key key,
    this.error,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var icon;
    var color;
    var message;

    switch (error) {
      case DomainError.invalidCredentials:
        icon = AppTheme.icons(type: IconsType.warning);
        color = AppTheme.colors(type: ColorsType.warning);
        message = AppTheme.errorMessage(type: ErrorType.unauthorized);
        break;
      default:
        icon = AppTheme.icons(type: IconsType.error);
        color = AppTheme.colors(type: ColorsType.error);
        message = 'Não foi possível realizar a busca';
        break;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Space(120),
        Container(
          child: Icon(
            icon,
            size: 60,
            color: color,
          ),
          margin: EdgeInsets.only(bottom: 16),
        ),
        Container(
          width: 150,
          child: Text(
            message,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}
