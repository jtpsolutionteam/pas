import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:pas/ui/protocols/protocols.dart';

class TextfieldSearch extends StatefulWidget {
  final bool isVisible;
  final FocusNode focus;
  final TextEditingController controller;
  final TypeTextFieldSearch type;
  final Function onSearch;

  const TextfieldSearch({
    Key key,
    @required this.isVisible,
    @required this.focus,
    @required this.controller,
    @required this.type,
    @required this.onSearch,
  }) : super(key: key);

  @override
  _TextfieldSearchState createState() => _TextfieldSearchState();
}

class _TextfieldSearchState extends State<TextfieldSearch> {
  @override
  Widget build(BuildContext context) {
    String label;
    String placeholder;
    TextCapitalization txtCapitalization;
    TextInputType txtInputType;

    switch (widget.type) {
      case TypeTextFieldSearch.batch:
        label = 'Insira o número do lote';
        placeholder = 'Ex.: 96532';
        txtCapitalization = TextCapitalization.none;
        txtInputType = TextInputType.number;
        break;
      case TypeTextFieldSearch.bl:
      default:
        label = 'Insira o código bl';
        placeholder = 'Ex.: AMKGL245500199B';
        txtCapitalization = TextCapitalization.characters;
        txtInputType = TextInputType.text;
        break;
    }

    return Visibility(
      visible: widget.isVisible,
      child: TextField(
        focusNode: widget.focus,
        controller: widget.controller,
        textCapitalization: txtCapitalization,
        keyboardType: txtInputType,
        autofocus: false,
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
          labelText: label,
          hintText: placeholder,
          suffixIcon: IconButton(
            icon: Icon(Icons.search),
            onPressed: () async => widget.onSearch(),
          ),
        ),
      ),
    );
  }
}
