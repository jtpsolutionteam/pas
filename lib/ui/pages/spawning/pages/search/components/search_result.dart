import 'package:flutter/material.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/pages/pages.dart';
import 'package:pas/ui/protocols/protocols.dart';
import 'package:provider/provider.dart';

import 'components.dart';

class SearchResult extends StatefulWidget {
  final Future<List<BatchEntity>> ship;

  const SearchResult({
    Key key,
    @required this.ship,
  }) : super(key: key);

  @override
  _SearchResultState createState() => _SearchResultState();
}

class _SearchResultState extends State<SearchResult> {
  @override
  Widget build(BuildContext context) {
    final searchPageNavigateTo = Provider.of<SearchPageNavigateTo>(context);

    return FutureBuilder<List<BatchEntity>>(
      future: widget.ship,
      builder: (context, snapshot) {
        if (snapshot.hasError) return MessageSearchError(error: snapshot.error);

        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return _notFounded();
            break;
          case ConnectionState.waiting:
            return _loading();
            break;
          case ConnectionState.active:
            break;
          case ConnectionState.done:
            var listShips = snapshot.data;

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  "Resultados de pesquisa",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Space(32),
                listShips == null || listShips?.length == 0
                    ? _notFounded()
                    : ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: listShips.length,
                        itemBuilder: (context, index) {
                          return BatchSearchItem(
                            searchPageNavigateTo: searchPageNavigateTo,
                            batch: snapshot.data[index],
                          );
                        },
                      ),
              ],
            );
            break;
        }

        return EmptyWidget();
      },
    );
  }

  Widget _notFounded() {
    return Text(
      'Nada foi encontrado, tente novamente!',
      style: TextStyle(fontSize: 16),
    );
  }

  Widget _loading() {
    return CircularProgressIndicator();
  }
}
