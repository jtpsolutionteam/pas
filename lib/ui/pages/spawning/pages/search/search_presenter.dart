import 'package:meta/meta.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/domain/usecases/usecases.dart';

abstract class SearchPresenter {
  Stream<String> get navigateToStream;

  Future<List<BatchEntity>> search({
    @required String parameter,
    @required SearchBatchParams typeParameter,
    int durationInSeconds,
  });

  void dispose();
}
