abstract class BatchSavePresenter {
  Future<bool> saveImages({List<String> imagesInBase64});
  void dispose();
}
