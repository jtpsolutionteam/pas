import 'package:flutter/material.dart';

enum ImageSaveMessageState { error, loading, success }

class ImageSaveMessage extends StatelessWidget {
  final ImageSaveMessageState state;

  const ImageSaveMessage({
    Key key,
    @required this.state,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var colorText;
    var colorBackground;
    var text;

    if (state == ImageSaveMessageState.error) {
      colorText = Color(0xFFFF0F0F);
      colorBackground = Color(0x10FF0F0F);
      text = "Falha ao salvar as fotos, tente novamente em alguns segundos!";
    } else if (state == ImageSaveMessageState.success) {
      colorText = Color(0xFF1BB55C);
      colorBackground = Color(0x101BB55C);
      text = "Fotos salvas com sucesso!";
    }

    if (state == ImageSaveMessageState.loading) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 8),
        margin: EdgeInsets.only(top: 16, bottom: 16),
        alignment: Alignment.center,
        child: SizedBox(
          width: 32,
          height: 32,
          child: CircularProgressIndicator(),
        ),
      );
    }

    return Container(
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 8),
      margin: EdgeInsets.only(top: 16, bottom: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: colorBackground,
      ),
      child: Center(
        child: Text(
          text,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: colorText),
        ),
      ),
    );
  }
}
