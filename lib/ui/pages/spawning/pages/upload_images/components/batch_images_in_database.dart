import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/pages/pages.dart';
import 'package:pas/ui/theme/theme.dart';
import 'package:pas/utils/utils.dart';

class BatchImagesInDatabase extends StatelessWidget {
  final SpawningWithImagesModel batch;
  final UploadSpawningImagesPresenter presenter;

  const BatchImagesInDatabase({
    Key key,
    @required this.batch,
    @required this.presenter,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      padding: EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 24,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(4)),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: GestureDetector(
              onTap: () {
                Get.to(DetailsUploadImagePage(imagesInBase64: batch.imagesInBase64));
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _blCode(batch.bl),
                  Space(6),
                  _date(batch.dateCreate),
                  Space(12),
                  _quantityPhotos(batch.imagesInBase64.length),
                ],
              ),
            ),
          ),
          Expanded(
            child: IconButton(
              icon: Icon(
                Icons.delete,
                color: AppTheme.colors(
                  type: ColorsType.error,
                ),
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext builder) {
                    return AlertDialog(
                      title: Text('Atenção'),
                      content: Text('Você realmente deseja excluir esse conjunto de fotos do seu celular?'),
                      actions: [_yesExcludeBtn(context), _noExcludeBtn(context)],
                    );
                  },
                ).then((value) {
                  if (value == true) {
                    presenter.delete(batch.id);
                  }
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _yesExcludeBtn(BuildContext context) {
    return TextButton(
      child: Text("Sim"),
      onPressed: () => Navigator.pop(context, true),
    );
  }

  Widget _noExcludeBtn(BuildContext context) {
    return TextButton(
      child: Text("Não"),
      onPressed: () => Navigator.pop(context, false),
    );
  }

  Widget _blCode(String text) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget _date(String text) {
    return Text(
      'Data de criação: ' + returnFormattedDate(text),
      style: TextStyle(
        fontSize: 14,
        color: Colors.grey[700],
      ),
    );
  }

  Widget _quantityPhotos(int quantity) {
    return Text(
      (quantity).toString() + ' foto(s)',
      style: TextStyle(
        fontSize: 14,
        color: Colors.grey[800],
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
