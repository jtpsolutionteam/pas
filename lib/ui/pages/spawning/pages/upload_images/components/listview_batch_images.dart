import 'package:flutter/material.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/ui/pages/pages.dart';

import 'components.dart';

class ListViewBatchImages extends StatelessWidget {
  final List<SpawningWithImagesModel> list;
  final UploadSpawningImagesPresenter presenter;

  const ListViewBatchImages({
    Key key,
    this.list,
    this.presenter,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return BatchImagesInDatabase(
            batch: list[index],
            presenter: presenter,
          );
        },
      ),
    );
  }
}
