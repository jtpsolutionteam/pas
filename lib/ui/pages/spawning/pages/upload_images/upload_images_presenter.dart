import 'package:pas/domain/models/models.dart';
import 'package:pas/ui/pages/upload_images/upload_images.dart';

abstract class UploadSpawningImagesPresenter extends UploadImagesPresenter {
  Future<bool> uploadAllImages(List<SpawningWithImagesModel> list);
}
