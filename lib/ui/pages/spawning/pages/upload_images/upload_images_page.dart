import 'package:flutter/material.dart';
import 'package:pas/domain/models/models.dart';
import 'package:pas/domain/helpers/domain_error.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/pages/pages.dart';

import 'components/components.dart';

class UploadSpawningImagesPage extends StatefulWidget {
  final UploadSpawningImagesPresenter presenter;

  const UploadSpawningImagesPage({
    Key key,
    @required this.presenter,
  }) : super(key: key);

  @override
  _UploadSpawningImagesPageState createState() => _UploadSpawningImagesPageState();
}

class _UploadSpawningImagesPageState extends State<UploadSpawningImagesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Upload de imagens'),
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      body: Builder(builder: (context) {
        widget.presenter.isDeleteSuccess.listen((event) {
          if (event == true) {
            SnackBarMessage.show(
              context: context,
              type: DomainError.none,
              message: "Imagens deletadas com sucesso!",
            );
            setState(() {});
          }
        });

        widget.presenter.isUploadAllSuccess.listen((event) {
          if (event == true) {
            SnackBarMessage.show(
              context: context,
              type: DomainError.none,
              message: "As imagens foram enviadas com sucesso!",
            );
            setState(() {});
          }
        });

        handleLoading(context, widget.presenter.isLoading);

        return FutureBuilder<List<SpawningWithImagesModel>>(
          future: widget.presenter.index(),
          builder: (context, snapshot) {
            if (snapshot.hasError) return EmptyWidget();

            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return NoPendingUploads();
                break;
              case ConnectionState.active:
                break;
              case ConnectionState.waiting:
                return _loading();
                break;
              case ConnectionState.done:
                if (snapshot.data.length == 0) return NoPendingUploads();

                return Padding(
                  padding: EdgeInsets.only(left: 12, right: 12, top: 24, bottom: 60),
                  child: Column(
                    children: [
                      MaxWidth(
                        child: ElevatedButton(
                          onPressed: () async {
                            if (snapshot.data != null && snapshot.data.length != 0) {
                              await widget.presenter.uploadAllImages(snapshot.data);
                            }
                          },
                          child: Text('Enviar fotos'),
                        ),
                      ),
                      Space(32),
                      ListViewBatchImages(
                        list: snapshot.data,
                        presenter: widget.presenter,
                      )
                    ],
                  ),
                );
                break;
            }
            return EmptyWidget();
          },
        );
      }),
    );
  }

  Widget _loading() {
    return Center(child: CircularProgressIndicator());
  }
}
