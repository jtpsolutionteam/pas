import 'package:flutter/material.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/pages/pages.dart';

import 'batch_images_presenter.dart';

class BatchImagesPage extends StatelessWidget {
  final BatchEntity batch;
  final BatchImagesPresenter presenter;

  const BatchImagesPage({
    Key key,
    @required this.batch,
    @required this.presenter,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(batch.txBl),
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        padding: EdgeInsets.only(left: 12, right: 12, top: 24, bottom: 60),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BatchInformation(batch: batch),
            Text(
              "Imagens",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            Space(24),
            FutureBuilder<AccountEntity>(
              future: presenter.loadAccount(),
              builder: (context, snapshot) {
                if (snapshot.hasError) return EmptyWidget();

                switch (snapshot.connectionState) {
                  case ConnectionState.done:
                    return GridBatchImagesNetwork(
                      accessToken: snapshot.data.token,
                      baseUrl: apiDownloadImages(),
                      batchs: batch,
                    );
                    break;
                  case ConnectionState.none:
                    break;
                  case ConnectionState.waiting:
                    return CircularProgressIndicator();
                    break;
                  case ConnectionState.active:
                    break;
                }
                return EmptyWidget();
              },
            ),
          ],
        ),
      ),
    );
  }
}
