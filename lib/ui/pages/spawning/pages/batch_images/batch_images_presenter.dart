import 'package:pas/domain/entities/entities.dart';

abstract class BatchImagesPresenter {
  Future<AccountEntity> loadAccount();
}
