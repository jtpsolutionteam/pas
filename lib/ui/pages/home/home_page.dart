import 'package:flutter/material.dart';
import 'package:pas/ui/components/components.dart';

class HomePage extends StatefulWidget {
  final List<ListTileCustom> listTileDrawer;

  const HomePage({
    Key key,
    @required this.listTileDrawer,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColorDark,
        title: Text('Portal Amalog de Serviços'),
        centerTitle: true,
      ),
      body: InitialPage(),
      drawer: Drawer(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColorDark,
              ),
              padding: EdgeInsets.symmetric(vertical: 16),
              width: double.infinity,
              height: 220,
              child: Image(
                image: AssetImage(
                  'lib/ui/assets/images/amalog-white.png',
                ),
              ),
            ),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: widget.listTileDrawer.length,
              itemBuilder: (context, index) {
                return widget.listTileDrawer[index];
              },
            ),
          ],
        ),
      ),
    );
  }
}

class InitialPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(),
    );
  }
}
