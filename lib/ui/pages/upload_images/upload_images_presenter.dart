import 'package:meta/meta.dart';

abstract class UploadImagesPresenter {
  Stream<String> get navigateToStream;
  Stream<bool> get isUploadSuccess;
  Stream<bool> get isUploadAllSuccess;
  Stream<bool> get isDeleteSuccess;
  Stream<bool> get isLoading;

  Future<bool> uploadImages({
    @required List<String> imagesInBase64,
    @required int cdProposta,
    @required int idElement,
    int duration = 0,
  });

  Future<bool> delete(int id);
  Future<dynamic> index();
  void dispose();
}
