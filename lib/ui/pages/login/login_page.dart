import 'package:flutter/material.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/pages/pages.dart';

import 'components/components.dart';

class LoginPage extends StatelessWidget {
  final LoginPresenter presenter;

  const LoginPage(this.presenter);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Builder(
        builder: (context) {
          handleLoading(context, presenter.isLoadingStream);
          handleNavigation(presenter.navigateToStream, clear: true);

          presenter.mainErrorStream.listen((error) {
            if (error != null) {
              SnackBarMessage.show(
                context: context,
                type: error,
                message: error.description,
              );
            }
          });

          return GestureDetector(
            onTap: () => hideKeyboard(context),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  LoginAmalog(),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 32, horizontal: 12),
                    width: double.infinity,
                    child: Form(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          LoginFormHeader(),
                          LoginFormBody(presenter: presenter),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
