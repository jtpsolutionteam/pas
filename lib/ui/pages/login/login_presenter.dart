import 'package:pas/domain/helpers/helpers.dart';

abstract class LoginPresenter {
  Stream<String> get emailErrorStream;
  Stream<String> get passwordErrorStream;
  Stream<bool> get isFormValidStream;
  Stream<DomainError> get mainErrorStream;
  Stream<String> get navigateToStream;
  Stream<bool> get isLoadingStream;

  void validateEmail(String email);
  void validatePassword(String password);
  void dispose();

  Future<void> auth();  
}
