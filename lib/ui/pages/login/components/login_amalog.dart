import 'package:flutter/material.dart';

class LoginAmalog extends StatelessWidget {
  const LoginAmalog({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 280,
      width: double.infinity,
      color: Theme.of(context).primaryColorDark,
      child: Center(
        child: Image(
          width: 200,
          height: 200,
          image: AssetImage('lib/ui/assets/images/amalog-white.png'),
        ),
      ),
    );
  }
}
