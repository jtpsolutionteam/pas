import 'package:flutter/material.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/ui/pages/pages.dart';

class LoginFormBody extends StatelessWidget {
  const LoginFormBody({
    Key key,
    @required this.presenter,
  }) : super(key: key);

  final LoginPresenter presenter;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        StreamBuilder<String>(
          stream: presenter.emailErrorStream,
          builder: (context, snapshot) {
            return TextField(
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                labelText: 'Email',
                errorText: snapshot.data?.isEmpty == true ? null : snapshot.data,
              ),
              onChanged: presenter.validateEmail,
            );
          },
        ),
        Space(24),
        StreamBuilder<String>(
          stream: presenter.passwordErrorStream,
          builder: (context, snapshot) {
            return TextField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: true,
              decoration: InputDecoration(
                labelText: 'Senha',
                errorText: snapshot.data?.isEmpty == true ? null : snapshot.data,
              ),
              onChanged: presenter.validatePassword,
            );
          },
        ),
        Space(24),
        SizedBox(
          width: double.maxFinite,
          child: StreamBuilder<bool>(
            stream: presenter.isFormValidStream,
            builder: (context, snapshot) {
              return ElevatedButton(
                onPressed: snapshot.data == true ? presenter.auth : null,
                child: Text(
                  'Entrar'.toUpperCase(),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}