import 'package:flutter/material.dart';
import 'package:pas/ui/components/components.dart';

class LoginFormHeader extends StatelessWidget {
  const LoginFormHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Login", style: Theme.of(context).textTheme.headline1),
        Space(12),
        Container(
          width: 225,
          child: Text(
            "Para entrar, insira seu email e sua senha.",
            style: TextStyle(fontSize: 16, color: Colors.grey),
          ),
        ),
        Space(40),
      ],
    );
  }
}