import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CardImageNetworkProtected extends StatelessWidget {
  final String baseUrl;
  final String imagePath;
  final Function onTap;
  final String accountToken;

  const CardImageNetworkProtected({
    Key key,
    @required this.baseUrl,
    @required this.imagePath,
    @required this.accountToken,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: CachedNetworkImage(
        imageUrl: baseUrl + imagePath,
        httpHeaders: {'Authorization': 'Bearer $accountToken'},
        fit: BoxFit.cover,
        placeholder: (context, url) => Center(
          child: CircularProgressIndicator(),
        ),
        errorWidget: (context, url, error) => Center(
          child: Icon(Icons.broken_image_outlined),
        ),
      ),
      onTap: onTap,
    );
  }
}
