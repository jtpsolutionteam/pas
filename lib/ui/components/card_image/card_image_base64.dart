import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';

class CardImageBase64File extends StatelessWidget {
  final String imageInBase64;
  final Function onTap;

  const CardImageBase64File({
    Key key,
    @required this.imageInBase64,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Uint8List bytes = base64Decode(imageInBase64);

    return GestureDetector(
      child: Image.memory(
        bytes,
        width: double.infinity,
        height: 150,
        fit: BoxFit.cover,
      ),
      onTap: onTap,
    );
  }
}