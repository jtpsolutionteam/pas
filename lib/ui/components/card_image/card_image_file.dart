import 'dart:io';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class CardImageFile extends StatelessWidget {
  final String imagePath;
  final Function onTap;

  const CardImageFile({
    Key key,
    @required this.imagePath,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Image.file(
        File(imagePath),
        width: double.infinity,
        height: 150,
        fit: BoxFit.cover,
      ),
      onTap: onTap,
    );
  }
}


