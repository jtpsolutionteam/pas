import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class ListTileCustom extends StatelessWidget {
  final String title;
  final Function onClick;
  final IconData icon;

  const ListTileCustom({
    Key key,
    @required this.title,
    @required this.onClick,
    @required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(icon, size: 24),
      title: Text(title),
      trailing: Icon(Icons.arrow_forward_ios, size: 16),
      focusColor: Color(0x200151B5),
      hoverColor: Color(0xFF0151B5),
      onTap: onClick,
      enabled: true,
    );
  }
}
