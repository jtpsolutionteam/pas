import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/components/components.dart';
import 'package:pas/utils/utils.dart';

class GridBatchImagesNetwork extends StatelessWidget {
  final String accessToken;
  final String baseUrl;
  final BatchEntity batchs;

  const GridBatchImagesNetwork({
    Key key,
    @required this.accessToken,
    @required this.baseUrl,
    @required this.batchs,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<ListPropostasDocto> filter = [];

    batchs.listPropostasDoctos.forEach((item) {
      if (returnOnlyImagePath(item.txUrl) == item.txUrl) {
        filter.add(item);
      }
    });

    if (filter.length == 0) {
      return EmptyWidget();
    }

    return GridView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: gridTemplate(),
      itemCount: filter.length,
      itemBuilder: (context, index) {
        var list = batchs.listPropostasDoctos;

        if (list.length == 0) return EmptyWidget();

        var url = apiDownloadImages() + filter[index].txUrl;

        return CardImageNetworkProtected(
          accountToken: accessToken,
          baseUrl: apiDownloadImages(),
          imagePath: filter[index].txUrl,
          onTap: () => Get.to(DetailsNetworkImagePage(
            imagePath: url,
            authorization: accessToken,
          )),
        );
      },
    );
  }
}
