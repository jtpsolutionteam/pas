import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/ui/components/components.dart';

import 'grid_flex.dart';

class GridBatchImagesBase64 extends StatelessWidget {
  final List<String> imagesInBase64;

  const GridBatchImagesBase64({
    Key key,
    @required this.imagesInBase64,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: gridTemplate(),
      itemCount: imagesInBase64.length,
      itemBuilder: (context, index) {
        if (imagesInBase64.length == 0) return EmptyWidget();

        return CardImageBase64File(
          imageInBase64: imagesInBase64[index],
          onTap: () => Get.to(DetailsImageBase64Page(
            imageinBase64: imagesInBase64[index],
          )),
        );
      },
    );
  }
}
