import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';

class DetailsImageBase64Page extends StatelessWidget {
  final String imageinBase64;

  const DetailsImageBase64Page({
    Key key,
    @required this.imageinBase64,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Uint8List bytes = base64Decode(imageinBase64);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Detalhes da foto"),
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(12),
        child: Center(
          child: Image.memory(
            bytes,
            width: double.maxFinite,
          ),
        ),
      ),
    );
  }
}
