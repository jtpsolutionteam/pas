import 'dart:io';

import 'package:flutter/material.dart';

class DetailsImageFilePage extends StatelessWidget {
  final String image;

  const DetailsImageFilePage({
    Key key,
    @required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Detalhes da foto"),
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => Navigator.pop(context, true),
        label: Text('Excluir foto'),
        backgroundColor: Theme.of(context).errorColor,
        icon: Icon(Icons.cancel),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(12),
        child: Center(
          child: Image.file(
            File(image),
            width: double.maxFinite,
          ),
        ),
      ),
    );
  }
}
