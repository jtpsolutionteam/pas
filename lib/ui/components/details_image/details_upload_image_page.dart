import 'package:flutter/material.dart';
import 'package:pas/ui/components/components.dart';

class DetailsUploadImagePage extends StatelessWidget {
  final List<String> imagesInBase64;

  const DetailsUploadImagePage({
    Key key,
    @required this.imagesInBase64,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Fotos do dispositivo'),
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        padding: EdgeInsets.only(
          left: 12,
          right: 12,
          top: 24,
          bottom: 60,
        ),
        child: GridBatchImagesBase64(
          imagesInBase64: imagesInBase64,          
        ),
      ),
    );
  }
}
