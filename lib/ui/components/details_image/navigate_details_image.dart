import 'package:flutter/material.dart';
import 'package:pas/ui/components/components.dart';

Future navigateToDetailsImagePage(BuildContext context, String imagePath) async {
    return await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailsImageFilePage(image: imagePath),
      ),
    );
  }