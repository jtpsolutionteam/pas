import 'package:flutter/material.dart';

class DetailsNetworkImagePage extends StatelessWidget {
  final String imagePath;
  final String authorization;

  const DetailsNetworkImagePage({
    Key key,
    this.imagePath,
    this.authorization,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Detalhes da foto"),
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(12),
        child: Center(
          child: Image(
            width: double.maxFinite,
            fit: BoxFit.contain,
            loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
              if (loadingProgress == null) return child;
              return Center(
                child: CircularProgressIndicator(
                  value: loadingProgress.expectedTotalBytes != null ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes : null,
                ),
              );
            },
            image: NetworkImage(
              imagePath,
              headers: {'Authorization': 'Bearer $authorization'},
            ),
          ),
        ),
      ),
    );
  }
}
