import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:pas/domain/helpers/helpers.dart';
import 'package:pas/ui/helpers/helpers.dart';
import 'package:pas/ui/protocols/protocols.dart';
import 'package:pas/ui/theme/theme.dart';

class SnackBarMessage {
  static Color _backgroundColor(TypeSnackBarMessage type) {
    switch (type) {
      case TypeSnackBarMessage.info:
        return Colors.green;
      case TypeSnackBarMessage.error:
        return AppTheme.colors(type: ColorsType.error);
      case TypeSnackBarMessage.warning:
        return AppTheme.colors(type: ColorsType.warning);
      default:
        return Colors.green;
    }
  }

  static Color _textColor(TypeSnackBarMessage type) {
    switch (type) {
      default:
        return Colors.white;
    }
  }

  static TypeSnackBarMessage _domainErrorToTypeSnackBarMessage(DomainError error) {
    switch (error) {
      case DomainError.invalidCredentials:
        return TypeSnackBarMessage.warning;
      case DomainError.notFound:
        return TypeSnackBarMessage.error;
      case DomainError.unexpected:
        return TypeSnackBarMessage.error;
      case DomainError.none:
        return TypeSnackBarMessage.info;
      default:
        return TypeSnackBarMessage.info;
    }
  }

  static void show({
    @required BuildContext context,
    @required DomainError type,
    @required String message,
  }) {
    var typeError = _domainErrorToTypeSnackBarMessage(type);

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: _backgroundColor(typeError),
        content: Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: _textColor(typeError),
          ),
        ),
        duration: Duration(seconds: 6),
      ),
    );
  }
}
