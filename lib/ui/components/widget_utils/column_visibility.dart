import 'package:flutter/material.dart';

class ColumnVisibility extends StatelessWidget {
  final bool visible;
  final List<Widget> children;

  const ColumnVisibility({
    Key key,
    @required this.visible,
    this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: visible,
      child: Column(
        children: children,
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }
}