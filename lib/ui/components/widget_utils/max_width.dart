import 'package:flutter/material.dart';

class MaxWidth extends StatelessWidget {
  final Widget child;

  const MaxWidth({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: child,
    );
  }
}