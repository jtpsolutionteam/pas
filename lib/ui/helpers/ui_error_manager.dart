import 'package:flutter/material.dart';
import 'package:pas/domain/helpers/helpers.dart';

import '../components/components.dart';

void handleMainError(BuildContext context, Stream<DomainError> stream) {
  stream.listen((error) {
    if (error != DomainError.none) {
      SnackBarMessage.show(
        context: context,
        message: error.description,
        type: error,
      );
    }
  });
}
