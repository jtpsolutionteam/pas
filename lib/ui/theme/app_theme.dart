import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:pas/ui/helpers/helpers.dart';

class AppTheme {
  static Color colors({@required ColorsType type}) {
    switch (type) {
      case ColorsType.error:
        return Colors.red[800];
      case ColorsType.warning:
        return Color(0xFFFAAD14);
      case ColorsType.primary:
        return Color(0xFF0151B5);
      case ColorsType.secundary:
        return Color(0xFFFB5607);
      default:
        return Colors.black;
    }
  }

  static String errorMessage({@required ErrorType type}) {
    switch (type) {
      case ErrorType.notFound:
        return 'Nenhum resultado foi encontrado!';
      case ErrorType.unauthorized:
        return 'Acesso não autorizado, faça o login novamente!';
      default:
        return '';
    }
  }

  static IconData icons({@required IconsType type}) {
    switch (type) {
      case IconsType.error:
        return Icons.error_outline;
      case IconsType.warning:
        return Icons.warning;
      default:
        return Icons.brightness_1;
    }
  }

  static ThemeData theme() {
    return ThemeData(
      primaryColor: Color(0xFF0151B5),
      primaryColorLight: Color(0xFF2A5788),
      primaryColorDark: Color(0xFF0C2646),
      accentColor: Color(0xFFFB5607),
      errorColor: Colors.red[800],
      backgroundColor: Colors.white,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      textTheme: _textStyles(),
      inputDecorationTheme: _inputStyles(),
      buttonTheme: _buttonStyles(),
    );
  }

  static TextTheme _textStyles() {
    return TextTheme(
      headline1: TextStyle(
        fontSize: 32,
        color: Colors.black,
        fontWeight: FontWeight.bold,
      ),
      button: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),      
    );
  }

  static BoxDecoration cardShadow({
    Color backgroundColor,
    double radius,
  }) {
    return BoxDecoration(
      color: backgroundColor,
      borderRadius: BorderRadius.all(Radius.circular(radius)),
      boxShadow: [
        BoxShadow(
          color: Color(0x08000000),
          spreadRadius: 0,
          blurRadius: 4,
          offset: Offset(1, 1), // changes position of shadow
        ),
      ],
    );
  }

  static InputDecorationTheme _inputStyles() {
    return InputDecorationTheme(
      border: OutlineInputBorder(borderSide: BorderSide()),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey[200], width: 1),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xFF0151B5), width: 1),
      ),
    );
  }

  static ButtonThemeData _buttonStyles() {
    return ButtonThemeData(
      colorScheme: ColorScheme.light(primary: Color(0xFF0151B5)),
      buttonColor: Color(0xFF0151B5),
      splashColor: Color(0xFF0151B5),
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 8),
      textTheme: ButtonTextTheme.primary,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
    );
  }
}
