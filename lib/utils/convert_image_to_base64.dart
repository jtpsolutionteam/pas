import 'dart:convert';
import 'dart:io' as Io;

import 'package:meta/meta.dart';

Future<String> convertImageToBase64({@required String image}) async {
  try {
    final bytes = await Io.File(image).readAsBytes();
    return base64Encode(bytes);
  } catch (error) {
    throw Exception('Não foi possível converter para base64');
  }
}

Future<List<String>> convertImagesToBase64(List<String> images) async {
  try {
    List<String> imagesConverted = [];

    for (var i = 0; i < images.length; i++) {
      var base64 = await convertImageToBase64(image: images[i]);
      imagesConverted.add(base64);
    }

    return imagesConverted;
  } catch (error) {
    throw Exception('Não foi possível converter para base64 todas as imagens');
  }
}
