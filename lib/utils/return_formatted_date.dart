/// Return example: 30/10/2001 às 20h00
String returnFormattedDate(String date) {
  try {
    var datetime = DateTime.parse(date);

    var day = (datetime.day).toString().padLeft(2, '0');
    var month = (datetime.month).toString().padLeft(2, '0');
    var year = (datetime.year).toString().padLeft(2, '0');
    var hour = (datetime.hour).toString().padLeft(2, '0');
    var minutes = (datetime.minute).toString().padLeft(2, '0');

    return "$day/$month/$year às $hour" + 'h' + '$minutes';
  } catch (error) {
    return '';
  }
}
