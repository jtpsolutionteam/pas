enum Protocol { http, https }

Protocol retrieveProtocolFromUrl(String url) {
  //example: https://rbmelolima.com.br
  try {
    var split = url.split('://');
    var protocol = split[0];

    if (protocol == 'https') {
      return Protocol.https;
    } else if (protocol == 'http') {
      return Protocol.http;
    }

    return null;
  } catch (error) {
    throw Exception('Não foi possível retornar um protocolo');
  }
}
