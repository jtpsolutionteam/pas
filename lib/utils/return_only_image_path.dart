String returnOnlyImagePath(String path) {
  try {
    final supportImages = ['JPEG', 'JPG', 'PNG', 'GIF', 'WEBP', 'BMP', 'WBMP'];
    final toUpper = path.toUpperCase();

    var isAImage = false;

    supportImages.forEach((supportType) {
      if (toUpper.endsWith(supportType)) {
        isAImage = true;
        return;
      }
    });

    return isAImage ? path : null;
  } catch (error) {
    throw Exception('Não foi possível determinar se o caminho tem ou não uma imagem válida');
  }
}
