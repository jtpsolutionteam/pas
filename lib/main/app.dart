import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/theme/theme.dart';

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Portal Amalog de Serviços',
      debugShowCheckedModeBanner: false,
      theme: AppTheme.theme(),
      initialRoute: '/',
      getPages: [
        GetPage(name: '/', page: makeSplashPage),
        GetPage(name: '/login', page: makeLoginPage),
        GetPage(name: '/home', page: makeHomePage),
        GetPage(name: '/spawning', page: makeSpawningPage),
        GetPage(name: '/list_shipment', page: makeIndexShipmentPageFactory),
      ],
    );
  }
}
