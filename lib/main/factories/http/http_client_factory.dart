import 'package:http/http.dart';
import 'package:pas/data/http/http.dart';
import 'package:pas/infra/http/http.dart';

HttpClient makeHttpAdapter() {
  final client = Client();
  return HttpAdapter(client);
}
