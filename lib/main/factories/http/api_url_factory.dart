String apiLoginUrl() => 'http://homologacao.amalog.com.br/oauth/token?';

String apiUploadImages() => 'http://app.amalog.com.br/appfoto/proposta/gravarfotosdesova';

String apiUploadShipmentImages() => 'http://app.amalog.com.br/appfoto/proposta/gravarfotoscarregamento';

String apiDownloadImages() => 'http://app.amalog.com.br/appfoto';

String apiUrlSearchBatchWithBlCode() => 'http://homologacao.amalog.com.br/appfoto/proposta/';

String apiUrlSearchBatchWithBatchCode() => 'http://homologacao.amalog.com.br/appfoto/proposta/lote/';

String apiUrlIndexShipment() => "http://app.amalog.com.br/appfoto/proposta/listarcarregamento";
