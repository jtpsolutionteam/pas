import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/main/factories/factories.dart';

LoadCurrentAccount makeLocalLoadCurrentAccount() {
  return LocalLoadCurrentAccount(makeLocalStorageAdapter());
}
