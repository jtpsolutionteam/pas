import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/main/factories/factories.dart';

IndexShipment makeRemoteIndexShipmentFactory() {
  return RemoteIndexShipment(
    httpClient: makeHttpAdapter(),
    fetchCache: makeLocalLoadCurrentAccount(),
    url: apiUrlIndexShipment(),
  );
}
