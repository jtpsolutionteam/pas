import 'package:pas/data/usecases/upload_images/remote_upload_images.dart';
import '../factories.dart';

RemoteUploadImages makeRemoteUploadSpawningImages() {
  return RemoteUploadImages(
    fetchCache: makeLocalLoadCurrentAccount(),
    httpClient: makeHttpAdapter(),
    url: apiUploadImages(),
  );
}

RemoteUploadImages makeRemoteUploadShipmentImages() {
  return RemoteUploadImages(
    fetchCache: makeLocalLoadCurrentAccount(),
    httpClient: makeHttpAdapter(),
    url: apiUploadShipmentImages(),
  );
}

