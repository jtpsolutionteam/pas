import 'package:get/get.dart';
import 'package:pas/domain/usecases/usecases.dart';

import 'usecases.dart';

void exitAppFactory() {
  RemoveCurrentAccount removeAccount = makeLocalRemoveCurrentAccount();
  removeAccount.remove();
  
  Get.offAllNamed('/login');
}
