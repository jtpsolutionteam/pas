import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/usecases/usecases.dart';

import '../factories.dart';

RemoveCurrentAccount makeLocalRemoveCurrentAccount() {
  return LocalRemoveCurrentAccount(removeSecureCacheStorage: makeLocalStorageAdapter());
}
