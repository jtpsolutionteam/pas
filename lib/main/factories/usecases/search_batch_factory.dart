import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/main/factories/http/http.dart';

SearchBatch makeRemoteSearchBatch() {
  return RemoteSearchBatch(
    httpClient: makeHttpAdapter(),
    fetchCache: makeLocalLoadCurrentAccount(),
    urlToBL: apiUrlSearchBatchWithBlCode(),
    urlToBatch: apiUrlSearchBatchWithBatchCode(),
  );
}
