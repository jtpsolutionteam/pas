import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/main/factories/factories.dart';

SaveCurrentAccount makeLocalSaveCurrentAccount() {
  return LocalSaveCurrentAccount(
    saveSecureCacheStorage: makeLocalStorageAdapter()
  );
}
