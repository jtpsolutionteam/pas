import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/infra/database/database.dart';

InsertSpawningImages makeInsertSpawningImagesFactory() {
  SpawningImagesDao spawningImagesDao = SpawningImagesDao();
  return LocalInsertSpawningImages(spawningImagesDao);
}

IndexSpawningImages makeIndexSpawningImagesFactory() {
  SpawningImagesDao spawningImagesDao = SpawningImagesDao();
  return LocalIndexSpawningImages(spawningImagesDao);
}

DeleteSpawningImages makeDeleteSpawningImagesFactory() {
  SpawningImagesDao spawningImagesDao = SpawningImagesDao();
  return LocalDeleteSpawningImages(spawningImagesDao);
}
