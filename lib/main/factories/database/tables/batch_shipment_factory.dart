import 'package:pas/data/usecases/usecases.dart';
import 'package:pas/domain/usecases/usecases.dart';
import 'package:pas/infra/database/database.dart';

InsertBatchShipmentImages makeInsertBatchShipmentImagesFactory() {
  BatchShipmentDao table = BatchShipmentDao();
  return LocalInsertBatchShipmentImages(table);
}

IndexBatchShipmentImages makeGetBatchShipmentFactory() {
  BatchShipmentDao table = BatchShipmentDao();
  return LocalIndexBatchShipmentImages(table);
}

DeleteBatchShipmentImages makeDeleteBatchShipmentFactory() {
  BatchShipmentDao table = BatchShipmentDao();
  return LocalDeleteBatchShipmentImages(table);
}
