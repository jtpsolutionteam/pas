import 'package:pas/main/factories/factories.dart';
import 'package:pas/presentation/presenters/presenters.dart';
import 'package:pas/ui/pages/pages.dart';

import '../../factories.dart';

UploadImagesShipmentPresenter makeUploadShipmentImagesPresenterFactory() {
  return GetxUploadImageShipmentPresenter(
    uploadSpawningImages: makeRemoteUploadShipmentImages(),
    getSpawningImages: makeGetBatchShipmentFactory(),
    deleteSpawningImages: makeDeleteBatchShipmentFactory(),
    removeCurrentAccount: makeLocalRemoveCurrentAccount(),
  );
}
