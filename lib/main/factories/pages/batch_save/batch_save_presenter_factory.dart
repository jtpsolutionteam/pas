import 'package:meta/meta.dart';
import 'package:pas/domain/entities/entities.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/presentation/presenters/presenters.dart';
import 'package:pas/ui/pages/pages.dart';

BatchSavePresenter makeBatchSavePresenterFactory({@required BatchEntity batch}) {
  return GetxBatchSavePresenter(
    saveSpawningImages: makeInsertSpawningImagesFactory(),
    batch: batch,
  );
}
