import 'package:pas/domain/entities/entities.dart';
import 'package:pas/presentation/presenters/presenters.dart';
import 'package:pas/ui/pages/pages.dart';

import '../../factories.dart';

BatchShipmentPresenter makeGetxBatchShipmentPresenterFactory(
  ListCarregamentoLote batchShipment,
) {
  return GetxBatchShipmentPresenter(
    save: makeInsertBatchShipmentImagesFactory(),
    batchShipment: batchShipment,
  );
}
