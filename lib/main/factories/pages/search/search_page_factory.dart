import 'package:flutter/material.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/pages/pages.dart';
import 'package:pas/ui/protocols/protocols.dart';

Widget makeSearchPage(SearchPageNavigateTo searchPageNavigateTo) {
  return SearchPage(
    key: UniqueKey(),
    presenter: makeSearchPagePresenter(),
    searchPageNavigateTo: searchPageNavigateTo,
  );
}
