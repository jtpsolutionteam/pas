import 'package:pas/main/factories/factories.dart';
import 'package:pas/presentation/presenters/presenters.dart';
import 'package:pas/ui/pages/pages.dart';

SearchPresenter makeSearchPagePresenter() {
  return GetxSearchPresenter(
    removeCurrentAccount: makeLocalRemoveCurrentAccount(),
    searchBatch: makeRemoteSearchBatch(),
  );
}
