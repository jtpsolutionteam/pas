import 'package:pas/main/builders/builders.dart';
import 'package:pas/presentation/protocols/protocols.dart';
import 'package:pas/validation/protocols/protocols.dart';
import 'package:pas/validation/validators/validators.dart';

Validation makeLoginValidation() {
  return ValidationComposite(makeLoginValidations());
}

List<FieldValidation> makeLoginValidations() {
  return [
    ...ValidationBuilder.field('email').required().email().build(),
    ...ValidationBuilder.field('password').required().build(),
  ];
}
