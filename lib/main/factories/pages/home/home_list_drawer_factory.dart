import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pas/ui/components/components.dart';

import '../../factories.dart';

List<ListTileCustom> makeListDrawer() {
  return [
    ListTileCustom(
      title: 'Carregamento',
      icon: Icons.directions_car,
      onClick: () async => Get.toNamed('/list_shipment'),
    ),
    ListTileCustom(
      title: 'Desova',
      icon: Icons.cloud_download,
      onClick: () async => Get.toNamed('/spawning'),
    ),
    ListTileCustom(
      title: 'Sair',
      icon: Icons.exit_to_app,
      onClick: () => exitAppFactory(),
    )
  ];
}
