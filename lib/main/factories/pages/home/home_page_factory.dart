import 'package:flutter/material.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/pages/pages.dart';

Widget makeHomePage() {
  return HomePage(listTileDrawer: makeListDrawer());
}
