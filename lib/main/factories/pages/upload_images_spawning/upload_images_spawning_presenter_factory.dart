import 'package:pas/main/factories/factories.dart';
import 'package:pas/presentation/presenters/presenters.dart';
import 'package:pas/ui/pages/pages.dart';

UploadSpawningImagesPresenter makeUploadImagesSpawningPresenterFactory() {
  return GetxUploadImageSpawningPresenter(
    uploadSpawningImages: makeRemoteUploadSpawningImages(),
    getSpawningImages: makeIndexSpawningImagesFactory(),
    deleteSpawningImages: makeDeleteSpawningImagesFactory(),
    removeCurrentAccount: makeLocalRemoveCurrentAccount(),
  );
}
