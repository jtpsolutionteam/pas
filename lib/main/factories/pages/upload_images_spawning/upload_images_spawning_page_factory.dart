import 'package:flutter/material.dart';
import 'package:pas/main/factories/factories.dart';
import 'package:pas/ui/pages/pages.dart';

Widget makeUploadImagesSpawningPageFactory() {
  return UploadSpawningImagesPage(
    presenter: makeUploadImagesSpawningPresenterFactory(),
  );
}
