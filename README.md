# PAS - Portal Amalog de Serviços

Este projeto foi escrito em Flutter.

Antes de tudo, cheque conforme a documentação se seu ambiente de desenvolvimento está devidamente configurado.
  - Ambiente configurado com variáveis de sistema;
  - SDK instalado;
  - Emulador ou dispositivo físico instalado e pronto para uso.


```bash
$ Clonando o repositório
git clone https://rbmelolima__@bitbucket.org/jtpsolutionteam/pas.git

$ Vá até a pasta
cd pas

$ Baixe todas as dependências
flutter run pub get

$ Inicie o projeto
flutter run
```

## Significados
- Batch = Lote
- Shipment = Carregamento
- Spawning = Desova
- Batch Shipment = Lote de carregamento
- Upload Images Spawning = Upload de fotos de desova
- Upload Images Shipment = Upload de fotos de carregamento